package com.synoit.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.naming.NamingException;

public class DBHandler {
	
	/**
	 * Gets the connection.
	 *
	 * @return the connection
	 * @throws SQLException the SQL exception
	 * @throws NamingException the naming exception
	 */
	public  Connection getConnection() throws SQLException, ClassNotFoundException {
		 Class.forName("com.mysql.jdbc.Driver");  
		 Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/synoit","root","");
		 return conn;
	}
	/**
	 * Close connection.
	 *
	 * @param con the con
	 * @throws SQLException the SQL exception
	 */
	public void closeConnection(Connection con) throws SQLException {
			if (con != null) {
				con.close();
			}
	}

	/**
	 * Close prepare statement.
	 *
	 * @param pstmt the pstmt
	 * @throws SQLException the SQL exception
	 */
	public void closePrepareStatement(PreparedStatement pstmt) throws SQLException{
			if (pstmt != null) {
				pstmt.close();
			}
	}

	/**
	 * Close resultset.
	 *
	 * @param rset the rset
	 * @throws SQLException the SQL exception
	 */
	public void closeResultset(ResultSet rset) throws SQLException {
			if (rset != null) {
				rset.close();
			}
	}

}
