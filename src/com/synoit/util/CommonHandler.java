package com.synoit.util;

import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.synoit.entity.GenerateReport;

/**
 * The Class CommonHandler.
 */
public class CommonHandler {

	/**
	 * Not null empty.
	 *
	 * @param value the value
	 * @return the boolean
	 */
	public static Boolean NotNullEmpty(String value) {
		if (null != value && !value.isEmpty()) {
			return true;
		}
		return false;
	}
	
	/**
	 * Creates the pdf.
	 *
	 * @param reportData the report data
	 * @return true, if successful
	 * @throws IOException             Signals that an I/O exception has occurred.
	 * @throws DocumentException             the document exception
	 * @throws SQLException             the SQL exception
	 */
	public boolean  createPdf(GenerateReport reportData, ResultSet rs) throws IOException, DocumentException, SQLException {
		Document document = new Document();
		StringBuilder file = new StringBuilder();
		StringBuilder reprortHeader = new StringBuilder();
		PdfWriter writer = null;
		try{
		
		file.append("Timetable - ");
		file.append(reportData.getDepartment());
		file.append(" ");
		file.append(reportData.getSemister());
		file.append(".pdf");
		writer = PdfWriter.getInstance(document, new FileOutputStream(file.toString()));
		document.open();
		reprortHeader.append("KSKES'S \n Gautam Polytechnic Institute, Gautamnagar. \n ");
		reprortHeader.append("TIME-TABLE MASTER COPY ");
		reprortHeader.append(reportData.getDepartment().toUpperCase());
		reprortHeader.append(" ");
		reprortHeader.append(reportData.getSemister().toUpperCase());
		reprortHeader.append("\n \n \n");
		Paragraph paragraphOne = new Paragraph(reprortHeader.toString());
		Font ffont = new Font(Font.FontFamily.TIMES_ROMAN, 5, Font.ITALIC);
		paragraphOne.setFont(ffont);
		paragraphOne.getFont().setColor(BaseColor.GRAY);
		paragraphOne.setAlignment(Element.ALIGN_CENTER);
		paragraphOne.getFont().setSize(12);
		document.add(paragraphOne);
		PdfPTable table = new PdfPTable(7);
		table.setWidthPercentage(100);
		table.setSpacingBefore(7f);
		table.setSpacingAfter(7f);

		Phrase timePhrase = new Phrase("Time");
		Phrase monPhrase = new Phrase("Monday");
		Phrase tuePhrase = new Phrase("Tuesday");
		Phrase wedPhrase = new Phrase("Wednesday");
		Phrase thuPhrase = new Phrase("Thursday");
		Phrase friPhrase = new Phrase("Friday");
		Phrase saTPhrase = new Phrase("Saturday");
		//Phrase sunPhrase = new Phrase("Sunday");
		PdfPCell timeHCell = getCellForHeader(timePhrase, BaseColor.LIGHT_GRAY);
		PdfPCell monHCell = getCellForHeader(monPhrase, BaseColor.LIGHT_GRAY);
		PdfPCell tueHCell = getCellForHeader(tuePhrase, BaseColor.LIGHT_GRAY);
		PdfPCell wedHCell = getCellForHeader(wedPhrase, BaseColor.LIGHT_GRAY);
		PdfPCell thuHCell = getCellForHeader(thuPhrase, BaseColor.LIGHT_GRAY);
		PdfPCell friHCell = getCellForHeader(friPhrase, BaseColor.LIGHT_GRAY);
		PdfPCell satHCell = getCellForHeader(saTPhrase, BaseColor.LIGHT_GRAY);
		//PdfPCell sunHCell = getCellForHeader(sunPhrase,BaseColor.WHITE);

		table.addCell(timeHCell);
		table.addCell(monHCell);
		table.addCell(tueHCell);
		table.addCell(wedHCell);
		table.addCell(thuHCell);
		table.addCell(friHCell);
		table.addCell(satHCell);
		//table.addCell(sunHCell);
		table.setHeaderRows(1);
		//int i = 0;
		while (rs.next()) {
			if(!"12:00 to 12:45".equals(rs.getString("Time")) && !"02:45 to 03:00".equals(rs.getString("Time"))){
				Phrase pTime = new Phrase(rs.getString("Time"));
				Phrase pMon = new Phrase(rs.getString("Mon"));
				Phrase pTue = new Phrase(rs.getString("Tue"));
				Phrase pWed = new Phrase(rs.getString("Wed"));
				Phrase pThu = new Phrase(rs.getString("Thu"));
				Phrase pFri = new Phrase(rs.getString("Fri"));
				Phrase pSat = new Phrase(rs.getString("Sat"));
				//Phrase ps= new Phrase(rs.getString("Sun"));
				getCellProperty(table, pTime);
				getCellProperty(table, pMon);
				getCellProperty(table, pTue);
				getCellProperty(table, pWed);
				getCellProperty(table, pThu);
				getCellProperty(table, pFri);
				getCellProperty(table, pSat);
				//getCellProperty(table, ps);
			}else{
				Phrase pTime = new Phrase(rs.getString(""));
				Phrase pMon = new Phrase(rs.getString(""));
				Phrase pTue = new Phrase(rs.getString(""));
				Phrase pWed = new Phrase(rs.getString("INTERVAL"));
				Phrase pThu = new Phrase(rs.getString(""));
				Phrase pFri = new Phrase(rs.getString(""));
				Phrase pSat = new Phrase(rs.getString(""));
				//Phrase ps= new Phrase(rs.getString("Sun"));
				getCellProperty(table, pTime);
				getCellProperty(table, pMon);
				getCellProperty(table, pTue);
				getCellProperty(table, pWed);
				getCellProperty(table, pThu);
				getCellProperty(table, pFri);
				getCellProperty(table, pSat);
				//getCellProperty(table, ps);
			}
	
		}
		document.add(table);
		}catch(Exception e){
			return false;
		}finally {
			document.close();
			writer.close();
		}
		return true;
	}

	/**
	 * Gets the cell for header.
	 *
	 * @param pp the pp
	 * @param color the color
	 * @return the cell for header
	 */
	private static PdfPCell getCellForHeader(Phrase pp, BaseColor color) {
		pp.getFont().setSize(10);
		PdfPCell cell1 = new PdfPCell(pp);
		cell1.setBackgroundColor(color);
		cell1.setHorizontalAlignment(Element.ALIGN_CENTER);
		cell1.setVerticalAlignment(Element.ALIGN_MIDDLE);
		return cell1;
	}

	/**
	 * Gets the cell property.
	 *
	 * @param table the table
	 * @param p the p
	 * @return the cell property
	 */
	private static void getCellProperty(PdfPTable table, Phrase p) {
		p.getFont().setSize(10);
		PdfPCell cell = new PdfPCell(p);
		cell.setPaddingLeft(2);
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		table.addCell(cell);
	}
	
	private static void getCellProperty( Phrase p, PdfPTable table) {
		p.getFont().setSize(10);
		PdfPCell cell = new PdfPCell(p);
		cell.setPaddingLeft(2);
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		//cell.setColspan(6);
		table.addCell(cell);
	}

}
