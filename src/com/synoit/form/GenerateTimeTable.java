package com.synoit.form;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;

import com.itextpdf.text.DocumentException;
import com.synoit.dao.ICreateTT;
import com.synoit.dao.ISearchInfo;
import com.synoit.dao.impl.SearchDBServiceImpl;
import com.synoit.dao.impl.TimeTableDBServiceImpl;
import com.synoit.entity.GenerateReport;
import com.synoit.util.CommonHandler;
import com.synoit.util.DBHandler;

import net.proteanit.sql.DbUtils;

public class GenerateTimeTable extends JFrame {

	private JPanel contentPane;
	private JTable table;
	private static String dept = null;
	private static String sem = null;
	private static ResultSet resultSet = null;
	ISearchInfo service = new SearchDBServiceImpl();
	ICreateTT ttSerive = new TimeTableDBServiceImpl();

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					GenerateTimeTable frame = new GenerateTimeTable();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 * @throws SQLException 
	 */
	public GenerateTimeTable() throws SQLException {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		//setBounds(100, 100, 953, 609);
		setBounds(0,0, Integer.MAX_VALUE,Integer.MAX_VALUE);
		setResizable(false);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblTimetableMaster = new JLabel("Time-Table Generator");
		lblTimetableMaster.setForeground(Color.WHITE);
		lblTimetableMaster.setHorizontalAlignment(SwingConstants.CENTER);
		lblTimetableMaster.setFont(new Font("Tahoma", Font.BOLD, 22));
		lblTimetableMaster.setBounds(504, 43, 281, 59);
		contentPane.add(lblTimetableMaster);
		
		JLabel lblNewLabel = new JLabel("");
		lblNewLabel.setForeground(Color.ORANGE);
		lblNewLabel.setFont(new Font("Tahoma", Font.BOLD, 13));
		lblNewLabel.setBounds(466, 289, 295, 38);
		lblNewLabel.setVisible(false);
		
		JLabel lblDepartment = new JLabel("Department");
		lblDepartment.setFont(new Font("Tahoma", Font.BOLD, 15));
		lblDepartment.setBounds(267, 218, 378, 23);
		contentPane.add(lblDepartment);
		
		
		
		JLabel lblSemister = new JLabel("Semister");
		lblSemister.setFont(new Font("Tahoma", Font.BOLD, 15));
		lblSemister.setBounds(562, 215, 74, 28);
		contentPane.add(lblSemister);
		
		JComboBox comboBox = new JComboBox();
		comboBox.setBounds(377, 220, 148, 20);
		for(String name : service.getDepartment()){
			comboBox.addItem(name);
		}
		
		contentPane.add(comboBox);
		
		JComboBox comboBox_1 = new JComboBox();
		comboBox_1.setBounds(646, 221, 148, 20);
		for(String name : service.getSemister()){
			comboBox_1.addItem(name);
		}
		
		contentPane.add(comboBox_1);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(251, 360, 856, 167);
		contentPane.add(scrollPane);
		
		table = new JTable();
		scrollPane.setViewportView(table);
		
		JButton btnSearch = new JButton("Search");
		btnSearch.setFont(new Font("Tahoma", Font.BOLD, 11));
		btnSearch.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setDept(comboBox.getSelectedItem().toString());
				setSem(comboBox_1.getSelectedItem().toString());
				dept = comboBox.getSelectedItem().toString();
				sem = comboBox_1.getSelectedItem().toString();
			boolean result = getTitmeTableInfo(dept,sem);
			}
		});
		btnSearch.setBounds(919, 183, 116, 28);
		contentPane.add(btnSearch);
		
		JButton btnHome = new JButton("Home");
		btnHome.setFont(new Font("Tahoma", Font.BOLD, 11));
		btnHome.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				new HomePageForm().setVisible(true);
				dispose();
			}
		});
		btnHome.setBounds(919, 132, 116, 28);
		contentPane.add(btnHome);
		
		JButton btnNewButton = new JButton("Update");
		btnNewButton.setFont(new Font("Tahoma", Font.BOLD, 11));
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					setDept(comboBox.getSelectedItem().toString());
					setSem(comboBox_1.getSelectedItem().toString());
					dept = comboBox.getSelectedItem().toString();
					sem = comboBox_1.getSelectedItem().toString();
					new CreateTimeTableForm(dept, sem).setVisible(true);
					dispose();
				} catch (SQLException e1) {
					e1.printStackTrace();
				}
			}
		});
		btnNewButton.setBounds(919, 235, 116, 28);
		contentPane.add(btnNewButton);
		
		JButton btnReport = new JButton("Export PDF");
		btnReport.setFont(new Font("Tahoma", Font.BOLD, 11));
		btnReport.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
				boolean result = generateTimeTableReport(dept, sem);
					
					if(result){
						lblNewLabel.setText("Time-Table Exported Sucessfully...!!!");
						lblNewLabel.setVisible(true);
					}else{
						lblNewLabel.setText("Error : Something went wrong ...!!!");
						lblNewLabel.setForeground(Color.RED);
						lblNewLabel.setVisible(true);
					}
				} catch (IOException | DocumentException e1) {
					e1.printStackTrace();
				}
			
			}
		});
		btnReport.setBounds(919, 289, 116, 28);
		contentPane.setBackground(new Color(51, 102, 153));
		contentPane.add(btnReport);
		
		
		contentPane.add(lblNewLabel);
		dept = comboBox.getSelectedItem().toString();
		sem = comboBox_1.getSelectedItem().toString();
		 getTitmeTableInfo( dept,  sem);
	
	}
	
private boolean  getTitmeTableInfo(String dpt, String sem){

		
		try {
			DBHandler dbHandler = new DBHandler();
			Connection conn = null;
			PreparedStatement preStmt = null; 
			ResultSet rs= null;
			try {
				conn = dbHandler.getConnection();
				rs= ttSerive.getTableInfo(conn, preStmt, rs, dpt, sem);
				table.setModel(DbUtils.resultSetToTableModel(rs));
				setResultSet(rs);
				while(rs.next()){
					setResultSet(rs);
					return true;
				}
			} catch (ClassNotFoundException e1) {
				e1.printStackTrace();
			}finally {
				dbHandler.closeResultset(rs);
				dbHandler.closePrepareStatement(preStmt);
				dbHandler.closeConnection(conn);
			}
		
		} catch (SQLException e1) {
			e1.printStackTrace();
		}
		
	return false;
	}

private boolean  generateTimeTableReport(String dpt, String sem) throws IOException, DocumentException{

	
	try {
		DBHandler dbHandler = new DBHandler();
		Connection conn = null;
		PreparedStatement preStmt = null; 
		ResultSet rs= null;
		try {
			conn = dbHandler.getConnection();
			rs= ttSerive.getTableInfo(conn, preStmt, rs, dpt, sem);
			//table.setModel(DbUtils.resultSetToTableModel(rs));
			CommonHandler handler = new CommonHandler();
			GenerateReport report = new GenerateReport();
			report.setDepartment(dpt);
			report.setSemister(sem);
			return handler.createPdf(report,rs);
			
		} catch (ClassNotFoundException e1) {
			e1.printStackTrace();
			return false;
		}finally {
			dbHandler.closeResultset(rs);
			dbHandler.closePrepareStatement(preStmt);
			dbHandler.closeConnection(conn);
		}
	
	} catch (SQLException e1) {
		e1.printStackTrace();
	}
	return false;
}

public static String getDept() {
	return dept;
}

public static void setDept(String dept) {
	GenerateTimeTable.dept = dept;
}

public static String getSem() {
	return sem;
}

public static void setSem(String sem) {
	GenerateTimeTable.sem = sem;
}

public static ResultSet getResultSet() {
	return resultSet;
}

public static void setResultSet(ResultSet resultSet) {
	GenerateTimeTable.resultSet = resultSet;
}
}
