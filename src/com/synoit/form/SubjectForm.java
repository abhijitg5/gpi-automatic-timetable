package com.synoit.form;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.table.TableColumn;
import javax.swing.table.TableModel;

import com.synoit.dao.ISearchInfo;
import com.synoit.dao.ISubject;
import com.synoit.dao.impl.SearchDBServiceImpl;
import com.synoit.dao.impl.SubjectDBServiceImpl;
import com.synoit.entity.Subject;
import com.synoit.util.DBHandler;

import net.proteanit.sql.DbUtils;

import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.awt.event.ActionEvent;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.SwingConstants;
import javax.swing.JTextField;
import javax.swing.JTable;
import javax.swing.JComboBox;
import javax.swing.JButton;
import javax.swing.JScrollPane;
import java.awt.Color;

public class SubjectForm extends JFrame {

	private JPanel contentPane;
	private JTextField textField;
	private JLabel lblSubjectCode;
	private JTextField textField_1;
	private JLabel lblDepartment;
	private JLabel lblNewLabel;
	private JComboBox comboBox;
	private JButton btnAdd;
	private JTable table;
	private JScrollPane scrollPane;
	private JMenuBar menuBar;
	private JMenu mnFile;
	private JMenuItem mntmUpdateSubject;
	private JMenuItem mntmDeleteSubject;
	private JMenuItem mntmExit;
	ISearchInfo service = new SearchDBServiceImpl();
	ISubject subjectService = new SubjectDBServiceImpl();
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					SubjectForm frame = new SubjectForm();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 * @throws SQLException 
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public SubjectForm() throws SQLException {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		//setBounds(100, 100, 754, 474);
		setBounds(0,0, Integer.MAX_VALUE,Integer.MAX_VALUE);
		setResizable(false);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(51, 102, 153));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblSubjectInformation = new JLabel("Add Subject Information");
		lblSubjectInformation.setForeground(Color.WHITE);
		lblSubjectInformation.setHorizontalAlignment(SwingConstants.CENTER);
		lblSubjectInformation.setFont(new Font("Tahoma", Font.BOLD, 22));
		lblSubjectInformation.setBounds(461, 22, 441, 78);
		contentPane.add(lblSubjectInformation);
		
		JLabel lblSubjectName = new JLabel("Subject Name");
		lblSubjectName.setFont(new Font("Tahoma", Font.BOLD, 13));
		lblSubjectName.setBounds(283, 155, 106, 30);
		contentPane.add(lblSubjectName);
		
		textField = new JTextField();
		textField.setBounds(414, 161, 176, 20);
		contentPane.add(textField);
		textField.setColumns(10);
		
		lblSubjectCode = new JLabel("Subject Code");
		lblSubjectCode.setFont(new Font("Tahoma", Font.BOLD, 13));
		lblSubjectCode.setBounds(283, 225, 106, 30);
		contentPane.add(lblSubjectCode);
		
		textField_1 = new JTextField();
		textField_1.setBounds(414, 231, 176, 20);
		contentPane.add(textField_1);
		textField_1.setColumns(10);
		
		lblDepartment = new JLabel("Department");
		lblDepartment.setFont(new Font("Tahoma", Font.BOLD, 13));
		lblDepartment.setBounds(282, 305, 82, 20);
		contentPane.add(lblDepartment);
		
		lblNewLabel = new JLabel("Semister");
		lblNewLabel.setFont(new Font("Tahoma", Font.BOLD, 13));
		lblNewLabel.setBounds(282, 374, 79, 20);
		contentPane.add(lblNewLabel);
		
		comboBox = new JComboBox();
		comboBox.setBounds(414, 375, 176, 20);
		for(String name : service.getSemister()){
			comboBox.addItem(name);
		}
		contentPane.add(comboBox);
		
		JComboBox comboBox_1 = new JComboBox();
		comboBox_1.setBounds(414, 306, 176, 20);
		for(String name : service.getDepartment()){
			comboBox_1.addItem(name);
		}
		contentPane.add(comboBox_1);
		
		btnAdd = new JButton("Add");
		btnAdd.setFont(new Font("Tahoma", Font.BOLD, 11));
		btnAdd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Subject subject = new Subject();
				subject.setSubName(textField.getText());
				subject.setSubCode(textField_1.getText());
				subject.setDept((String)comboBox_1.getSelectedItem());
				subject.setSemister((String)comboBox.getSelectedItem());
				try {
					subjectService.insertSubjectInfo(subject);
					getSubjectInfo();
					textField.setText("");
					textField_1.setText("");
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		btnAdd.setBounds(294, 466, 106, 30);
		contentPane.add(btnAdd);
		
		scrollPane = new JScrollPane();
		scrollPane.setBounds(678, 161, 550, 335);
		contentPane.add(scrollPane);
		
		table = new JTable();
		scrollPane.setViewportView(table);
		
		JButton btnGoToHome = new JButton("Home");
		btnGoToHome.setFont(new Font("Tahoma", Font.BOLD, 11));
		btnGoToHome.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				new HomePageForm().setVisible(true);
				dispose();
			}
		});
		btnGoToHome.setBounds(434, 466, 137, 30);
		contentPane.add(btnGoToHome);
		
		menuBar = new JMenuBar();
		menuBar.setBounds(0, 0, 97, 21);
		contentPane.add(menuBar);
		
		mnFile = new JMenu("File");
		menuBar.add(mnFile);
		
		mntmUpdateSubject = new JMenuItem("Update Subject");
		mntmUpdateSubject.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			try {
				new UpdateSubjectForm().setVisible(true);
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
			dispose();
			}
		});
		mnFile.add(mntmUpdateSubject);
		
		mntmDeleteSubject = new JMenuItem("Delete Subject");
		mntmDeleteSubject.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					new DeleteSubjectForm().setVisible(true);
				} catch (SQLException e1) {
					e1.printStackTrace();
				}
				dispose();
			}
		});
		mnFile.add(mntmDeleteSubject);
		
		mntmExit = new JMenuItem("Exit");
		mntmExit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.exit(0);
			}
		});
		mnFile.add(mntmExit);
		
		getSubjectInfo();
	}
	
	private void getSubjectInfo(){

	
		try {
			DBHandler dbHandler = new DBHandler();
			Connection conn = null;
			PreparedStatement preStmt = null; 
			ResultSet rs= null;
			try {
				conn = dbHandler.getConnection();
				
				rs= service.getSubjectInfo(conn, preStmt, rs);
				table.setModel(DbUtils.resultSetToTableModel(rs));
				
			} catch (ClassNotFoundException e1) {
				e1.printStackTrace();
			}finally {
				dbHandler.closeResultset(rs);
				dbHandler.closePrepareStatement(preStmt);
				dbHandler.closeConnection(conn);
			}
		
		} catch (SQLException e1) {
			e1.printStackTrace();
		}
		
	
	}
}
