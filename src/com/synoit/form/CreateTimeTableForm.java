package com.synoit.form;

import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;

import com.synoit.dao.ICreateTT;
import com.synoit.dao.ISearchInfo;
import com.synoit.dao.impl.SearchDBServiceImpl;
import com.synoit.dao.impl.TimeTableDBServiceImpl;
import com.synoit.entity.Department;
import com.synoit.entity.Faculty;
import com.synoit.entity.Subject;
import com.synoit.entity.TimeTable;
import com.synoit.util.DBHandler;

import net.proteanit.sql.DbUtils;
import java.awt.Color;

public class CreateTimeTableForm extends JFrame {

	private JPanel contentPane;
	private JTable table;
	ISearchInfo service = new SearchDBServiceImpl();
	ICreateTT ttSerive = new TimeTableDBServiceImpl();
	
	private  static String  department= null;
	private  static String  semister= null;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					CreateTimeTableForm frame = new CreateTimeTableForm("","");
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 * @throws SQLException 
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public CreateTimeTableForm(String dept, String sem) throws SQLException {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		//setBounds(100, 100, 951, 710);
		setBounds(0,0, Integer.MAX_VALUE,Integer.MAX_VALUE);
		//setResizable(false);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(51, 102, 153));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblManageTimetable = new JLabel("Time-Table Manager");
		lblManageTimetable.setForeground(Color.WHITE);
		lblManageTimetable.setFont(new Font("Tahoma", Font.BOLD, 22));
		lblManageTimetable.setHorizontalAlignment(SwingConstants.CENTER);
		lblManageTimetable.setBounds(421, 0, 520, 73);
		contentPane.add(lblManageTimetable);
		
		JLabel lblSelectDepartment = new JLabel("Department");
		lblSelectDepartment.setFont(new Font("Tahoma", Font.BOLD, 13));
		lblSelectDepartment.setBounds(107, 125, 89, 29);
		contentPane.add(lblSelectDepartment);
		
		JComboBox comboBox = new JComboBox();
		comboBox.setBounds(194, 126, 146, 29);
	/*	for(String name : service.getDepartment()){
			comboBox.addItem(name);
		}*/
		comboBox.addItem(dept);
		comboBox.addItem(getDepartment());
		contentPane.add(comboBox);
		
		JLabel lblSelectSemister = new JLabel("Semister");
		lblSelectSemister.setFont(new Font("Tahoma", Font.BOLD, 13));
		lblSelectSemister.setBounds(370, 124, 100, 30);
		contentPane.add(lblSelectSemister);
		
		JComboBox comboBox_1 = new JComboBox();
		comboBox_1.setBounds(461, 128, 154, 24);
		comboBox_1.addItem(sem);
	/*	for(String name : service.getSemister()){
			comboBox_1.addItem(name);
		}*/
		contentPane.add(comboBox_1);
		
		JLabel lblSelectFaculty = new JLabel("Faculty");
		lblSelectFaculty.setFont(new Font("Tahoma", Font.BOLD, 13));
		lblSelectFaculty.setBounds(118, 224, 78, 29);
		contentPane.add(lblSelectFaculty);
		
		JComboBox comboBox_2 = new JComboBox();
		comboBox_2.setBounds(194, 227, 146, 25);
		for(String faculty : service.getFaculty()){
			comboBox_2.addItem(faculty);
		}
		contentPane.add(comboBox_2);
		
		JLabel lblSelectDay = new JLabel(" Day");
		lblSelectDay.setFont(new Font("Tahoma", Font.BOLD, 13));
		lblSelectDay.setBounds(669, 223, 56, 30);
		contentPane.add(lblSelectDay);
		
		JComboBox comboBox_3 = new JComboBox();
		comboBox_3.setBounds(751, 227, 154, 25);
		for(String day : service.getDay()){
			comboBox_3.addItem(day);
		}
		contentPane.add(comboBox_3);
		
		JLabel lblTimeSlot = new JLabel("Time Slot");
		lblTimeSlot.setFont(new Font("Tahoma", Font.BOLD, 13));
		lblTimeSlot.setBounds(647, 129, 78, 21);
		contentPane.add(lblTimeSlot);
		
		JComboBox comboBox_4 = new JComboBox();
		comboBox_4.setBounds(751, 126, 154, 29);
		for(String slot : service.getTimeSlot()){
			comboBox_4.addItem(slot);
		}
		contentPane.add(comboBox_4);
		
		JLabel lblSubject = new JLabel("Subject");
		lblSubject.setFont(new Font("Tahoma", Font.BOLD, 13));
		lblSubject.setBounds(378, 228, 71, 20);
		contentPane.add(lblSubject);
		
		JComboBox comboBox_5 = new JComboBox();
		comboBox_5.setBounds(472, 226, 154, 26);
		for(String name : service.getSubject()){
			comboBox_5.addItem(name);
		}
		contentPane.add(comboBox_5);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(194, 382, 891, 232);
		contentPane.add(scrollPane);
		
		table = new JTable();
		scrollPane.setViewportView(table);
		
		JButton btnAdd = new JButton("Add");
		btnAdd.setFont(new Font("Tahoma", Font.BOLD, 11));
		btnAdd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				addOrUpdateTT(dept, sem, comboBox_3, comboBox_4, comboBox_5);
			}

			private void addOrUpdateTT(String dept, String sem, JComboBox comboBox_3, JComboBox comboBox_4,
					JComboBox comboBox_5) {
				TimeTable timeTable = new TimeTable();
				Department departMent = new Department();
				Faculty faculty = new Faculty();
				Subject subject=new Subject();
				subject.setSubCode(comboBox_5.getSelectedItem().toString());
				departMent.setName(dept);
				//faculty.setName(name);
				timeTable.setDepartment(departMent);
				timeTable.setFaculty(faculty);
				timeTable.setSemister(sem);
				timeTable.setSubject(subject);
				timeTable.setTimeSlot(comboBox_4.getSelectedItem().toString());
				timeTable.setDay(comboBox_3.getSelectedItem().toString());
				try {
					ttSerive.insertTimeTable(timeTable);
				} catch (SQLException e1) {
					e1.printStackTrace();
				}
				getTitmeTableInfo(dept,sem);
			}
		});
		btnAdd.setBounds(996, 184, 89, 29);
		contentPane.add(btnAdd);
		
		JButton btnUpdate = new JButton("Update");
		btnUpdate.setFont(new Font("Tahoma", Font.BOLD, 11));
		btnUpdate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
					addOrUpdateTT(dept, sem, comboBox_3, comboBox_4, comboBox_5);
				}

				private void addOrUpdateTT(String dept, String sem, JComboBox comboBox_3, JComboBox comboBox_4,
						JComboBox comboBox_5) {
					TimeTable timeTable = new TimeTable();
					Department departMent = new Department();
					Faculty faculty = new Faculty();
					Subject subject=new Subject();
					subject.setSubCode(comboBox_5.getSelectedItem().toString());
					departMent.setName(dept);
					//faculty.setName(name);
					timeTable.setDepartment(departMent);
					timeTable.setFaculty(faculty);
					timeTable.setSemister(sem);
					timeTable.setSubject(subject);
					timeTable.setTimeSlot(comboBox_4.getSelectedItem().toString());
					timeTable.setDay(comboBox_3.getSelectedItem().toString());
					try {
						ttSerive.insertTimeTable(timeTable);
					} catch (SQLException e1) {
						e1.printStackTrace();
					}
					getTitmeTableInfo(dept,sem);
				}
			}
		);
		btnUpdate.setBounds(996, 241, 89, 29);
		contentPane.add(btnUpdate);
		
		JButton btnDelete = new JButton("Delete");
		btnDelete.setFont(new Font("Tahoma", Font.BOLD, 11));
		btnDelete.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				addOrUpdateTT(dept, sem, comboBox_3, comboBox_4, comboBox_5);
			}

			private void addOrUpdateTT(String dept, String sem, JComboBox comboBox_3, JComboBox comboBox_4,
					JComboBox comboBox_5) {
				TimeTable timeTable = new TimeTable();
				Department departMent = new Department();
				Faculty faculty = new Faculty();
				Subject subject=new Subject();
				subject.setSubCode("");
				departMent.setName(dept);
				//faculty.setName(name);
				timeTable.setDepartment(departMent);
				timeTable.setFaculty(faculty);
				timeTable.setSemister(sem);
				timeTable.setSubject(subject);
				timeTable.setTimeSlot(comboBox_4.getSelectedItem().toString());
				timeTable.setDay(comboBox_3.getSelectedItem().toString());
				try {
					ttSerive.insertTimeTable(timeTable);
				} catch (SQLException e1) {
					e1.printStackTrace();
				}
				getTitmeTableInfo(dept,sem);
			}
		});
		btnDelete.setBounds(996, 306, 89, 29);
		contentPane.add(btnDelete);
		
		JButton btnHome = new JButton("Home");
		btnHome.setFont(new Font("Tahoma", Font.BOLD, 11));
		btnHome.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
				new HomePageForm().setVisible(true);
			}
		});
		btnHome.setBounds(996, 126, 89, 29);
		contentPane.add(btnHome);
		
		getTitmeTableInfo(dept,sem);
	}
	
private void getTitmeTableInfo(String dpt, String sem){

		
		try {
			DBHandler dbHandler = new DBHandler();
			Connection conn = null;
			PreparedStatement preStmt = null; 
			ResultSet rs= null;
			try {
				conn = dbHandler.getConnection();
				rs= ttSerive.getTableInfo(conn, preStmt, rs, dpt, sem);
				table.setModel(DbUtils.resultSetToTableModel(rs));
			} catch (ClassNotFoundException e1) {
				e1.printStackTrace();
			}finally {
				dbHandler.closeResultset(rs);
				dbHandler.closePrepareStatement(preStmt);
				dbHandler.closeConnection(conn);
			}
		
		} catch (SQLException e1) {
			e1.printStackTrace();
		}
		
	
	}

	public String getDepartment() {
		return department;
	}

	public void setDepartment(String department) {
		CreateTimeTableForm.department = department;
	}

	public String getSemister() {
		return semister;
	}

	public void setSemister(String semister) {
		CreateTimeTableForm.semister = semister;
	}


}
