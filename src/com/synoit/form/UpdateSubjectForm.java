package com.synoit.form;

import java.awt.Component;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;

import com.synoit.dao.ISearchInfo;
import com.synoit.dao.ISubject;
import com.synoit.dao.impl.SearchDBServiceImpl;
import com.synoit.dao.impl.SubjectDBServiceImpl;
import com.synoit.entity.Subject;
import com.synoit.util.CommonHandler;
import com.synoit.util.DBHandler;

import net.proteanit.sql.DbUtils;
import java.awt.Color;

@SuppressWarnings("serial")
public class UpdateSubjectForm extends JFrame {

	private JPanel contentPane;
	private JTable table;
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_2;
	
	ISearchInfo service = new SearchDBServiceImpl();
	ISubject subjectService = new SubjectDBServiceImpl();

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					UpdateSubjectForm frame = new UpdateSubjectForm();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 * @throws SQLException 
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public UpdateSubjectForm() throws SQLException {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		//setBounds(100, 100, 780, 455);
		setBounds(0,0, Integer.MAX_VALUE,Integer.MAX_VALUE);
		setResizable(false);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(51, 102, 153));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JPopupMenu popupMenu = new JPopupMenu();
		addPopup(contentPane, popupMenu);
		
		JTextPane txtpnSomeThingWent = new JTextPane();
		txtpnSomeThingWent.setText("Some thing went wrong");
		popupMenu.add(txtpnSomeThingWent);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(674, 164, 652, 336);
		contentPane.add(scrollPane);
		
		table = new JTable();
		scrollPane.setViewportView(table);
		
		JLabel lblUpdateSubjectInformation = new JLabel("Update Subject Information");
		lblUpdateSubjectInformation.setForeground(Color.WHITE);
		lblUpdateSubjectInformation.setHorizontalAlignment(SwingConstants.CENTER);
		lblUpdateSubjectInformation.setFont(new Font("Tahoma", Font.BOLD, 22));
		lblUpdateSubjectInformation.setBounds(401, 59, 319, 44);
		contentPane.add(lblUpdateSubjectInformation);
		
		JLabel lblSubject = new JLabel("Subject");
		lblSubject.setFont(new Font("Tahoma", Font.BOLD, 13));
		lblSubject.setBounds(313, 227, 90, 22);
		contentPane.add(lblSubject);
		
		JLabel lblDepartment = new JLabel("Department");
		lblDepartment.setFont(new Font("Tahoma", Font.BOLD, 13));
		lblDepartment.setBounds(313, 342, 90, 22);
		contentPane.add(lblDepartment);
		
		JLabel lblSemister = new JLabel("Semister");
		lblSemister.setFont(new Font("Tahoma", Font.BOLD, 13));
		lblSemister.setBounds(313, 398, 90, 22);
		contentPane.add(lblSemister);
		
		textField = new JTextField();
		textField.setBounds(424, 229, 160, 20);
		contentPane.add(textField);
		textField.setColumns(10);
		
		JComboBox comboBox = new JComboBox();
		comboBox.setBounds(424, 344, 158, 20);
		for(String dpt : service.getDepartment()){
			comboBox.addItem(dpt);
		}
		contentPane.add(comboBox);
		
		JComboBox comboBox_1 = new JComboBox();
		comboBox_1.setBounds(424, 400, 160, 20);
		for(String sem : service.getSemister()){
			comboBox_1.addItem(sem);
		}
		contentPane.add(comboBox_1);
		
		JButton btnNewButton = new JButton("Update");
		btnNewButton.setFont(new Font("Tahoma", Font.BOLD, 12));
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				Subject subject = new Subject();
				subject.setSubName(textField.getText());
				subject.setSubCode(textField_1.getText());
				if(null != textField_2.getText() && !textField_2.getText().isEmpty()){
					subject.setId(Integer.parseInt(textField_2.getText()));
				}
				subject.setDept((String)comboBox.getSelectedItem());
				subject.setSemister((String)comboBox_1.getSelectedItem());
				
				if(CommonHandler.NotNullEmpty(subject.getSubName()) 
						&& CommonHandler.NotNullEmpty(subject.getSubCode())
						&& null != subject.getId()) {
					try {
						boolean result = subjectService.updateSubjectInfo(subject);
						if (result) {
							getSubjectInfo();
						} else {

						}

						textField.setText("");
						textField_1.setText("");
						textField_2.setText("");
					} catch (SQLException e1) {
						e1.printStackTrace();
					}
				}else{
					System.out.println("Error : Invalid Request fields");
				}
			
			
			}
		});
		btnNewButton.setBounds(369, 468, 97, 32);
		contentPane.add(btnNewButton);
		
		JButton btnNewButton_1 = new JButton("Home");
		btnNewButton_1.setFont(new Font("Tahoma", Font.BOLD, 12));
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				new HomePageForm().setVisible(true);
				dispose();
			}
		});
		btnNewButton_1.setBounds(509, 468, 97, 32);
		contentPane.add(btnNewButton_1);
		
		JMenuBar menuBar = new JMenuBar();
		menuBar.setBounds(0, 0, 97, 21);
		contentPane.add(menuBar);
		
		JMenu mnNewMenu = new JMenu("File");
		menuBar.add(mnNewMenu);
		
		JMenuItem mntmAddSubject = new JMenuItem("Add Subject");
		mntmAddSubject.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					new SubjectForm().setVisible(true);
					dispose();
				} catch (SQLException e1) {
					e1.printStackTrace();
				}
			}
		});
		mnNewMenu.add(mntmAddSubject);
		
		JMenuItem mntmDeleteSubject = new JMenuItem("Delete Subject");
		mntmDeleteSubject.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					new DeleteSubjectForm().setVisible(true);
				} catch (SQLException e1) {
					e1.printStackTrace();
				}
				dispose();
			}
		});
		mnNewMenu.add(mntmDeleteSubject);
		
		JMenuItem mntmExit = new JMenuItem("Exit");
		mntmExit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.exit(0);
			}
		});
		mnNewMenu.add(mntmExit);
		
		JLabel lblSubjectCode = new JLabel("Subject Code");
		lblSubjectCode.setFont(new Font("Tahoma", Font.BOLD, 13));
		lblSubjectCode.setBounds(315, 287, 88, 22);
		contentPane.add(lblSubjectCode);
		
		textField_1 = new JTextField();
		textField_1.setBounds(424, 289, 160, 20);
		contentPane.add(textField_1);
		textField_1.setColumns(10);
		
		JLabel lblSrNo = new JLabel("Sr No");
		lblSrNo.setFont(new Font("Tahoma", Font.BOLD, 13));
		lblSrNo.setBounds(313, 161, 90, 32);
		contentPane.add(lblSrNo);
		
		textField_2 = new JTextField();
		textField_2.setBounds(424, 168, 160, 20);
		contentPane.add(textField_2);
		textField_2.setColumns(10);
		
		getSubjectInfo();
	}
	
	private void getSubjectInfo(){

		
		try {
			DBHandler dbHandler = new DBHandler();
			Connection conn = null;
			PreparedStatement preStmt = null; 
			ResultSet rs= null;
			try {
				conn = dbHandler.getConnection();
				
				rs= service.getSubjectInfo(conn, preStmt, rs);
				table.setModel(DbUtils.resultSetToTableModel(rs));
				
			} catch (ClassNotFoundException e1) {
				e1.printStackTrace();
			}finally {
				dbHandler.closeResultset(rs);
				dbHandler.closePrepareStatement(preStmt);
				dbHandler.closeConnection(conn);
			}
		
		} catch (SQLException e1) {
			e1.printStackTrace();
		}
		
	
	}
	private static void addPopup(Component component, final JPopupMenu popup) {
		component.addMouseListener(new MouseAdapter() {
			public void mousePressed(MouseEvent e) {
				if (e.isPopupTrigger()) {
					showMenu(e);
				}
			}
			public void mouseReleased(MouseEvent e) {
				if (e.isPopupTrigger()) {
					showMenu(e);
				}
			}
			private void showMenu(MouseEvent e) {
				popup.show(e.getComponent(), e.getX(), e.getY());
			}
		});
	}
}
