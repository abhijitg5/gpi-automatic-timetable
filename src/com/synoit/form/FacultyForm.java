package com.synoit.form;

import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;

import com.synoit.dao.IFaculty;
import com.synoit.dao.ISearchInfo;
import com.synoit.dao.impl.FacultyDBServiceImpl;
import com.synoit.dao.impl.SearchDBServiceImpl;
import com.synoit.entity.Faculty;
import com.synoit.util.DBHandler;

import net.proteanit.sql.DbUtils;
import javax.swing.JScrollPane;
import java.awt.Color;

public class FacultyForm extends JFrame {

	private JPanel contentPane;
	private JTextField textField;
	private JTable table;
	ISearchInfo service = new SearchDBServiceImpl();
	IFaculty fService = new FacultyDBServiceImpl();

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					FacultyForm frame = new FacultyForm();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 * @throws SQLException 
	 */
	@SuppressWarnings("unchecked")
	public FacultyForm() throws SQLException {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		//setBounds(100, 100, 823, 501);
		setBounds(0,0, Integer.MAX_VALUE,Integer.MAX_VALUE);
		setResizable(false);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(51, 102, 153));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblAddFacultyInformation = new JLabel("Faculty Information");
		lblAddFacultyInformation.setForeground(Color.WHITE);
		lblAddFacultyInformation.setHorizontalAlignment(SwingConstants.CENTER);
		lblAddFacultyInformation.setFont(new Font("Tahoma", Font.BOLD, 24));
		lblAddFacultyInformation.setBounds(461, 90, 372, 54);
		contentPane.add(lblAddFacultyInformation);
		
		JLabel lblFacultyName = new JLabel("Faculty Name");
		lblFacultyName.setFont(new Font("Tahoma", Font.BOLD, 13));
		lblFacultyName.setBounds(277, 263, 95, 24);
		contentPane.add(lblFacultyName);
		
		JLabel lblSubject = new JLabel("Subject");
		lblSubject.setFont(new Font("Tahoma", Font.BOLD, 13));
		lblSubject.setBounds(283, 383, 66, 30);
		contentPane.add(lblSubject);
		
		textField = new JTextField();
		textField.setBounds(376, 264, 145, 24);
		contentPane.add(textField);
		textField.setColumns(10);
		
		JComboBox comboBox = new JComboBox();
		comboBox.setBounds(376, 387, 145, 24);
		for(String name : service.getSubject()){
			comboBox.addItem(name);
		}
		contentPane.add(comboBox);
		
		JLabel lblDepartment = new JLabel("Department");
		lblDepartment.setFont(new Font("Tahoma", Font.BOLD, 13));
		lblDepartment.setBounds(277, 323, 95, 24);
		contentPane.add(lblDepartment);
		
		JComboBox comboBox_1 = new JComboBox();
		comboBox_1.setBounds(376, 326, 145, 21);
		for(String name : service.getDepartment()){
			comboBox_1.addItem(name);
		}
		contentPane.add(comboBox_1);
		
		JButton btnAdd = new JButton("Add");
		btnAdd.setFont(new Font("Tahoma", Font.BOLD, 11));
		btnAdd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Faculty faculty = new Faculty ();
				faculty.setName(textField.getText());
				faculty.setDept(comboBox_1.getSelectedItem().toString());
				faculty.setSubject(comboBox.getSelectedItem().toString());
				try {
					fService.insertFacultInfo(faculty);
					getFacultyInfo();
				} catch (SQLException e1) {
					e1.printStackTrace();
				}
			}
		});
		btnAdd.setBounds(326, 487, 89, 30);
		contentPane.add(btnAdd);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(604, 248, 611, 269);
		contentPane.add(scrollPane);
		
		table = new JTable();
		scrollPane.setViewportView(table);
		
		JMenuBar menuBar = new JMenuBar();
		menuBar.setBounds(0, 0, 97, 21);
		contentPane.add(menuBar);
		
		JMenu mnHome = new JMenu("Home");
		menuBar.add(mnHome);
		
		JMenuItem mntmGoToHome = new JMenuItem("Go to Home");
		mntmGoToHome.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
				new HomePageForm().setVisible(true);
			}
		});
		mnHome.add(mntmGoToHome);
		
		JButton btnHome = new JButton("Home");
		btnHome.setFont(new Font("Tahoma", Font.BOLD, 11));
		btnHome.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				new HomePageForm().setVisible(true);
				dispose();
			}
		});
		btnHome.setBounds(451, 487, 89, 30);
		contentPane.add(btnHome);
		getFacultyInfo();
	}
	
	private void getFacultyInfo(){

		
		try {
			DBHandler dbHandler = new DBHandler();
			Connection conn = null;
			PreparedStatement preStmt = null; 
			ResultSet rs= null;
			try {
				conn = dbHandler.getConnection();
				
				rs= service.getFacultyInfo(conn, preStmt, rs);
				table.setModel(DbUtils.resultSetToTableModel(rs));
				
			} catch (ClassNotFoundException e1) {
				e1.printStackTrace();
			}finally {
				dbHandler.closeResultset(rs);
				dbHandler.closePrepareStatement(preStmt);
				dbHandler.closeConnection(conn);
			}
		
		} catch (SQLException e1) {
			e1.printStackTrace();
		}
		
	
	}
}
