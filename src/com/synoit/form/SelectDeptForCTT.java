package com.synoit.form;

import java.awt.Color;
import java.awt.Component;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import org.eclipse.wb.swing.FocusTraversalOnArray;

import com.synoit.dao.ISearchInfo;
import com.synoit.dao.impl.SearchDBServiceImpl;

public class SelectDeptForCTT extends JFrame {

	private JPanel contentPane;

	ISearchInfo service = new SearchDBServiceImpl();
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					SelectDeptForCTT frame = new SelectDeptForCTT();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 * @throws SQLException 
	 */
	public SelectDeptForCTT() throws SQLException {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		//setBounds(100, 100, 604, 572);
		setBounds(0,0, Integer.MAX_VALUE,Integer.MAX_VALUE);
		setResizable(false);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(51, 102, 153));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblManageTimetable = new JLabel("Manage Time-Table");
		lblManageTimetable.setForeground(Color.WHITE);
		lblManageTimetable.setFont(new Font("Tahoma", Font.BOLD, 22));
		lblManageTimetable.setHorizontalAlignment(SwingConstants.CENTER);
		lblManageTimetable.setBounds(437, 52, 363, 68);
		contentPane.add(lblManageTimetable);
		
		JLabel lblDepartment = new JLabel("Department");
		lblDepartment.setFont(new Font("Dialog", Font.BOLD, 12));
		lblDepartment.setBounds(416, 184, 94, 30);
		contentPane.add(lblDepartment);
		
		JLabel lblSemister = new JLabel("Semister");
		lblSemister.setFont(new Font("Dialog", Font.BOLD, 12));
		lblSemister.setBounds(416, 258, 94, 30);
		contentPane.add(lblSemister);
		
		JComboBox comboBox = new JComboBox();
		comboBox.setBounds(546, 186, 176, 30);
		for(String name : service.getDepartment()){
			comboBox.addItem(name);
		}
		contentPane.add(comboBox);
		
		JComboBox comboBox_1 = new JComboBox();
		comboBox_1.setBounds(546, 264, 176, 30);
		for(String name : service.getSemister()){
			comboBox_1.addItem(name);
		}
		contentPane.add(comboBox_1);
		
		JButton btnHome = new JButton("Home");
		btnHome.setFont(new Font("Dialog", Font.BOLD, 12));
		btnHome.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				new HomePageForm().setVisible(true);
				dispose();
			}
		});
		btnHome.setBounds(636, 365, 108, 36);
		contentPane.add(btnHome);
		
		JButton btnManage = new JButton("Manage");
		btnManage.setFont(new Font("Dialog", Font.BOLD, 12));
		btnManage.addActionListener(new ActionListener() {
			@SuppressWarnings("static-access")
			public void actionPerformed(ActionEvent e) {
				
				try {
					
					CreateTimeTableForm cretaeTT = 	new CreateTimeTableForm(comboBox.getSelectedItem().toString(),comboBox_1.getSelectedItem().toString());
					cretaeTT.setVisible(true);
					//cretaeTT.setDepartment(comboBox.getSelectedItem().toString());
					//cretaeTT.setSemister(comboBox_1.getSelectedItem().toString());
					dispose();
				} catch (SQLException e1) {
					e1.printStackTrace();
				}
			}
		});
		btnManage.setBounds(508, 365, 99, 36);
		contentPane.add(btnManage);
		contentPane.setFocusTraversalPolicy(new FocusTraversalOnArray(new Component[]{lblManageTimetable, lblDepartment, lblSemister, comboBox, comboBox_1, btnHome, btnManage}));
	}
}
