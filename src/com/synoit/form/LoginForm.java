package com.synoit.form;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Font;
import javax.swing.SwingConstants;
import java.awt.Color;

public class LoginForm {

	private JFrame frame;
	private JTextField textField;
	//private JTextField textField_1;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					LoginForm window = new LoginForm();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public LoginForm() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.getContentPane().setBackground(new Color(51, 102, 153));
		frame.setBounds(450, 100, 511, 468);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblUserName = new JLabel("User Name");
		lblUserName.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblUserName.setBounds(103, 98, 82, 35);
		frame.getContentPane().add(lblUserName);
		
		JLabel lblPassword = new JLabel("Password");
		lblPassword.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblPassword.setBounds(103, 165, 73, 35);
		frame.getContentPane().add(lblPassword);
		
		textField = new JTextField();
		textField.setBounds(195, 107, 173, 20);
		frame.getContentPane().add(textField);
		textField.setColumns(10);
		
		//textField_1 = new JTextField();
		JPasswordField pfield = new JPasswordField();
		pfield.setBounds(195, 174, 173, 20);
		frame.getContentPane().add(pfield);
		pfield.setColumns(10);
		
		JLabel lblInvalidUserName = new JLabel("Invalid user name or password");
		lblInvalidUserName.setForeground(new Color(255, 127, 80));
		lblInvalidUserName.setFont(new Font("Tahoma", Font.BOLD, 13));
		lblInvalidUserName.setHorizontalAlignment(SwingConstants.CENTER);
		lblInvalidUserName.setBounds(186, 69, 230, 27);
		lblInvalidUserName.setVisible(false);
		
		JButton btnLogin = new JButton("Login");
		btnLogin.setFont(new Font("Tahoma", Font.BOLD, 13));
		btnLogin.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				if(null != textField.getText() && !textField.getText().isEmpty() && textField.getText().equals("admin")
						&& pfield.getText().equals("admin")){
					frame.dispose();
					new HomePageForm().setVisible(true);
				}else{
					lblInvalidUserName.setVisible(true);
				}
				
			}
		});
		btnLogin.setBounds(158, 262, 107, 35);
		frame.getContentPane().add(btnLogin);
		
		JButton btnCancel = new JButton("Cancel");
		btnCancel.setFont(new Font("Tahoma", Font.BOLD, 13));
		btnCancel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.exit(0);
			}
		});
		btnCancel.setBounds(304, 262, 112, 35);
		frame.getContentPane().add(btnCancel);
		
		JLabel lblLogin = new JLabel("Login");
		lblLogin.setHorizontalAlignment(SwingConstants.CENTER);
		lblLogin.setFont(new Font("Tahoma", Font.BOLD, 18));
		lblLogin.setBounds(186, 26, 147, 35);
		lblLogin.setForeground(new Color(255, 255, 153));
		
		frame.getContentPane().add(lblLogin);
		
		
		frame.getContentPane().add(lblInvalidUserName);
	}
}
