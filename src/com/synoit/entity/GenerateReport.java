package com.synoit.entity;

import java.sql.ResultSet;

public class GenerateReport {

	private String department;
	private String semister;
	private ResultSet rs;
	private String fileName;

	public String getDepartment() {
		return department;
	}

	public void setDepartment(String department) {
		this.department = department;
	}

	public String getSemister() {
		return semister;
	}

	public void setSemister(String semister) {
		this.semister = semister;
	}

	public ResultSet getRs() {
		return rs;
	}

	public void setRs(ResultSet rs) {
		this.rs = rs;
	}

	public String getFileName() {
		return department + "-" + semister;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	@Override
	public String toString() {
		return "GenerateReport [department=" + department + ", semister=" + semister + ", rs=" + rs + ", fileName="
				+ fileName + "]";
	}

	

}
