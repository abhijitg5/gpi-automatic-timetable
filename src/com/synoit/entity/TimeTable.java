package com.synoit.entity;

public class TimeTable {

	private Integer id;
	
	private Faculty faculty;
	
	private String timeSlot;
	
	private Subject subject;
	
	private AcadymicYear acadymicYear;
	
	private Department department;
	
	private String semister;

	private String day;
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Faculty getFaculty() {
		return faculty;
	}

	public void setFaculty(Faculty faculty) {
		this.faculty = faculty;
	}

	public String getTimeSlot() {
		return timeSlot;
	}

	public void setTimeSlot(String timeSlot) {
		this.timeSlot = timeSlot;
	}

	public Subject getSubject() {
		return subject;
	}

	public void setSubject(Subject subject) {
		this.subject = subject;
	}

	public AcadymicYear getAcadymicYear() {
		return acadymicYear;
	}

	public void setAcadymicYear(AcadymicYear acadymicYear) {
		this.acadymicYear = acadymicYear;
	}

	public Department getDepartment() {
		return department;
	}

	public void setDepartment(Department department) {
		this.department = department;
	}

	public String getSemister() {
		return semister;
	}

	public void setSemister(String semister) {
		this.semister = semister;
	}

	public String getDay() {
		return day;
	}

	public void setDay(String day) {
		this.day = day;
	}

	@Override
	public String toString() {
		return "TimeTable [id=" + id + ", faculty=" + faculty + ", timeSlot=" + timeSlot + ", subject=" + subject
				+ ", acadymicYear=" + acadymicYear + ", department=" + department + ", semister=" + semister + ", day="
				+ day + "]";
	}
	
}
