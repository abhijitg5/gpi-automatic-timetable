package com.synoit.entity;

public class AcadymicYear {

	private Integer id;
	
	private String yearName;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getYearName() {
		return yearName;
	}

	public void setYearName(String yearName) {
		this.yearName = yearName;
	}

	@Override
	public String toString() {
		return "AcadymicYear [id=" + id + ", yearName=" + yearName + "]";
	}
	
}
