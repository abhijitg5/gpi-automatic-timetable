package com.synoit.entity;

public class Faculty {

	private Integer id;
	
	private String name;
	
	private String dept;
	
	private String subject;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDept() {
		return dept;
	}

	public void setDept(String dept) {
		this.dept = dept;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	@Override
	public String toString() {
		return "Faculty [id=" + id + ", name=" + name + ", dept=" + dept + ", subject=" + subject + "]";
	}

	
	
}
