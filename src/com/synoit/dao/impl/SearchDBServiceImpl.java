package com.synoit.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.synoit.dao.ISearchInfo;
import com.synoit.util.DBHandler;

public class SearchDBServiceImpl extends DBHandler implements ISearchInfo {

	@Override
	public List<String> getDepartment() throws SQLException {
		Connection connection = null;
		PreparedStatement preStatement = null;
		ResultSet rs = null;
		List<String> listofDepartment = new ArrayList<>();
		try{
			connection  = getConnection();
			preStatement = connection.prepareStatement(QueryConstant.getDepartmentQuery());
			rs = preStatement.executeQuery();
			while(rs.next()) {
				listofDepartment.add(rs.getString("name"));
			}
			
		} catch(Exception exception){
			throw new SQLException();
		} finally {
			try {
				closeResultset(rs);
				closePrepareStatement(preStatement);
				closeConnection(connection);
			} catch (SQLException exception) {
				throw new SQLException();
			}
			
		}
		return listofDepartment;
	}

	@Override
	public List<String> getSemister() throws SQLException {
		Connection connection = null;
		PreparedStatement preStatement = null;
		ResultSet rs = null;
		List<String> listofDepartment = new ArrayList<>();
		try{
			connection  = getConnection();
			preStatement = connection.prepareStatement(QueryConstant.getSemister());
			rs = preStatement.executeQuery();
			while(rs.next()) {
				listofDepartment.add(rs.getString("semisterName"));
			}
			
		} catch(Exception exception){
			throw new SQLException();
		} finally {
			try {
				closeResultset(rs);
				closePrepareStatement(preStatement);
				closeConnection(connection);
			} catch (SQLException exception) {
				throw new SQLException();
			}
			
		}
		return listofDepartment;
	}

	@Override
	public ResultSet getSubjectInfo(Connection connection,PreparedStatement preStatement,ResultSet rs) throws SQLException {
		try{
			preStatement = connection.prepareStatement(QueryConstant.getSubjectInfo());
			rs = preStatement.executeQuery();
		} catch(Exception exception){
			throw new SQLException();
		} 
		return rs;
	}

	@Override
	public ResultSet getFacultyInfo(Connection connection, PreparedStatement preStatement, ResultSet rs)
			throws SQLException {
		try{
			preStatement = connection.prepareStatement(QueryConstant.getFacultyInfo());
			rs = preStatement.executeQuery();
		} catch(Exception exception){
			throw new SQLException();
		} 
		return rs;
	}

	@Override
	public List<String> getSubject() throws SQLException {
		Connection connection = null;
		PreparedStatement preStatement = null;
		ResultSet rs = null;
		List<String> listofDepartment = new ArrayList<>();
		try{
			connection  = getConnection();
			preStatement = connection.prepareStatement(QueryConstant.getSubject());
			rs = preStatement.executeQuery();
			while(rs.next()) {
				listofDepartment.add(rs.getString("subName"));
			}
			
		} catch(Exception exception){
			throw new SQLException();
		} finally {
			try {
				closeResultset(rs);
				closePrepareStatement(preStatement);
				closeConnection(connection);
			} catch (SQLException exception) {
				throw new SQLException();
			}
			
		}
		return listofDepartment;
	}

	@Override
	public List<String> getFaculty() throws SQLException {
		Connection connection = null;
		PreparedStatement preStatement = null;
		ResultSet rs = null;
		List<String> listofDepartment = new ArrayList<>();
		try{
			connection  = getConnection();
			preStatement = connection.prepareStatement(QueryConstant.getFaculty());
			rs = preStatement.executeQuery();
			while(rs.next()) {
				listofDepartment.add(rs.getString("name"));
			}
			
		} catch(Exception exception){
			throw new SQLException();
		} finally {
			try {
				closeResultset(rs);
				closePrepareStatement(preStatement);
				closeConnection(connection);
			} catch (SQLException exception) {
				throw new SQLException();
			}
			
		}
		return listofDepartment;
	}

	@Override
	public List<String> getDay() throws SQLException {
		List<String> dayList = new ArrayList<>();
		dayList.add("Monday");
		dayList.add("Tuesday");
		dayList.add("Wednesday");
		dayList.add("Thursday");
		dayList.add("Friday");
		dayList.add("Saturday");
		return dayList;
	}

	@Override
	public List<String> getTimeSlot() throws SQLException {
		List<String> timeSlot = new ArrayList<>();
		timeSlot.add("10:00 to 11:00");
		timeSlot.add("11:00 to 12:00");
		//timeSlot.add("12:00 to 12:45");
		timeSlot.add("12:45 to 01:45");
		timeSlot.add("01:45 to 02:45");
		//timeSlot.add("02:45 to 03:00");
		timeSlot.add("03:00 to 04:00");
		timeSlot.add("04:00 to 05:00");
		return timeSlot;
	}
	
	

}
