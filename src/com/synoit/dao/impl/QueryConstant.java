package com.synoit.dao.impl;

public class QueryConstant {

	public static String getDepartmentQuery(){
		return new StringBuilder()
				.append("SELECT * FROM t_department")
				.toString();
		
	}
	
	public static String getSemister(){
		return new StringBuilder()
				.append("SELECT * FROM t_semister")
				.toString();
		
	}
	
	public static String getSubject(){
		return new StringBuilder()
				.append("SELECT * FROM t_subject")
				.toString();
		
	}
	
	public static String getFaculty(){
		return new StringBuilder()
				.append("SELECT * FROM t_faculty")
				.toString();
		
	}
	
	public static String getSubjectInfo() {
		return new StringBuilder()
				.append("SELECT sub.id As SRNO, sub.subName As Subject, sub.subCode As SubCode ,")
				.append(" dpt.name As Dept , sem.semisterName As Semister FROM t_subject sub")
				.append("	JOIN t_department dpt ON sub.deptId = dpt.id ")
				.append("	JOIN t_semister sem ON sub.semId = sem.id").toString();

	}
	
	public static String getFacultyInfo() {
		return new StringBuilder()
				.append("SELECT f.id As SRNO, f.name As Name, f.department As Dept, f.subject AS Subject FROM t_faculty f").toString();

	}
	
/*	public static String getTimeTableInfo() {
		return new StringBuilder().append("SELECT tt.id As SRNO, tt.Time, CONCAT(tt.Mon, ',', tt.Mon_SF) AS Mon,")
				.append("CONCAT(tt.Tue, ',', tt.Tue_SF) AS Tue,")
				.append("  CONCAT(tt.Wed, ',', tt.Wed_SF) AS Wed,")
				.append("  CONCAT(tt.Thu, ',', tt.Thu_SF) AS Thu,")
				.append("  CONCAT(tt.Fri, ',', tt.Fri_SF) AS Fri,")
				.append("   CONCAT(tt.Sat, ',', tt.Sat_SF) AS Sat,")
				.append("   tt.Sun, tt.Department As Dept, tt.Semister As Sem")
				.append(" from t_timeTable_aa tt")
				.toString();

	}*/
	
	public static String getTimeTableInfo() {
		return new StringBuilder().append("SELECT  tt.Time, tt.Mon AS Mon,")
				.append(" tt.Tue  AS Tue,")
				.append("  tt.Wed  AS Wed,")
				.append("  tt.Thu  AS Thu,")
				.append("   tt.Fri  AS Fri,")
				.append("  tt.Sat  AS Sat,")
				.append("   tt.Sun, tt.Department As Dept, tt.Semister As Sem")
				.append(" from t_timeTable_aa tt where Department = ? and Semister = ?")
				.toString();

	}
	
	public static String insertIntoFaculty(){
		return new StringBuilder()
				.append("insert into t_faculty (name, department ,subject) values (?,?,?);")
				.toString();
		
	}
	
	public static String updateFaculty(){
		return new StringBuilder()
				.append("SELECT * FROM t_semister")
				.toString();
		
	}
	
	public static String deleteFaculty(){
		return new StringBuilder()
				.append("delete * FROM t_semister")
				.toString();
		
	}
	
	public static String insertIntoSubject(){
		return new StringBuilder()
				.append("insert into t_subject (subName, subCode, deptId, semId) value  (?, ?,  (select id from t_department where name =?),")
				.append("(select id from t_semister where semisterName =? ) );")
				.toString();
		
	}
	
	public static String updateSubject(){
		return new StringBuilder()
				.append("Update t_subject SET subName= ? , subCode = ?, deptId =  (select id from t_department where name =?), ")
				.append( " semId = (select id from t_semister where semisterName =? ) where id= ? ")
				.toString();
		
	}
	
	public static String deleteSubject(){
		return new StringBuilder()
				.append("Delete FROM t_semister where id = ?")
				.toString();
		
	}
	
	public static String updateTimeTableForMon(){
		return new StringBuilder()
				.append("update t_timeTable_aa  set Mon = ? where Time = ? and Department = ? and Semister = ?;")
				.toString();
		
	}
	
	public static String updateTimeTableForTue(){
		return new StringBuilder()
				.append("update t_timeTable_aa  set Tue = ? where Time = ? and Department = ? and Semister = ?;")
				.toString();
		
	}
	
	public static String updateTimeTableForWed(){
		return new StringBuilder()
				.append("update t_timeTable_aa  set Wed = ? where Time = ? and Department = ? and Semister = ?;")
				.toString();
		
	}
	
	public static String updateTimeTableForThu(){
		return new StringBuilder()
				.append("update t_timeTable_aa  set Thu = ? where Time = ? and Department = ? and Semister = ?;")
				.toString();
		
	}
	
	public static String updateTimeTableForFri(){
		return new StringBuilder()
				.append("update t_timeTable_aa  set Fri = ? where Time = ? and Department = ? and Semister = ?;")
				.toString();
		
	}
	
	public static String updateTimeTableForSat(){
		return new StringBuilder()
				.append("update t_timeTable_aa  set Sat = ? where Time = ? and Department = ? and Semister = ?;")
				.toString();
		
	}
	
	
}
