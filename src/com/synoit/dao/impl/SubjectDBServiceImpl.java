package com.synoit.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import com.synoit.dao.ISubject;
import com.synoit.entity.Subject;
import com.synoit.util.DBHandler;

public class SubjectDBServiceImpl extends DBHandler  implements ISubject{

	@Override
	public ResultSet insertSubjectInfo(Subject subject) throws SQLException {

		PreparedStatement preStatement = null;
		ResultSet rs = null;
		Connection connection = null;
		try {
			String query = "";
			connection  = getConnection();
			query = QueryConstant.insertIntoSubject();
			preStatement = connection.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
			preStatement.setString(1,subject.getSubName());
			preStatement.setString(2,subject.getSubCode());
			preStatement.setString(3,subject.getDept());
			preStatement.setString(4,subject.getSemister());
			preStatement.executeUpdate();
			rs = preStatement.getGeneratedKeys();

		} catch (Exception exception) {
			throw new SQLException();
		} finally {
			try {
				closeResultset(rs);
				closePrepareStatement(preStatement);
			} catch (SQLException exception) {
				throw new SQLException();
			}
		}
		return rs;
	
	}

	@Override
	public Boolean updateSubjectInfo(Subject subject) throws SQLException {

		PreparedStatement preStatement = null;
		ResultSet rs = null;
		Connection connection = null;
		try {
			String query = "";
			connection  = getConnection();
			query = QueryConstant.updateSubject();
			preStatement = connection.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
			preStatement.setString(1,subject.getSubName());
			preStatement.setString(2,subject.getSubCode());
			preStatement.setString(3,subject.getDept());
			preStatement.setString(4,subject.getSemister());
			preStatement.setInt(5,subject.getId());
			preStatement.executeUpdate();
			rs = preStatement.getGeneratedKeys();
			if(rs !=null ){
				return true;
			}

		} catch (Exception exception) {
			throw new SQLException();
		} finally {
			try {
				closeResultset(rs);
				closePrepareStatement(preStatement);
			} catch (SQLException exception) {
				throw new SQLException();
			}
		}
		return false;
	
	}

	@Override
	public Boolean deleteSubjectInfo(Subject subject) throws SQLException {

		PreparedStatement preStatement = null;
		ResultSet rs = null;
		Connection connection = null;
		try {
			String query = "";
			connection  = getConnection();
			query = QueryConstant.deleteSubject();
			preStatement = connection.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
			preStatement.setInt(1,subject.getId());
			preStatement.executeUpdate();
			rs = preStatement.getGeneratedKeys();
			if(rs !=null ){
				return true;
			}

		} catch (Exception exception) {
			throw new SQLException();
		} finally {
			try {
				closeResultset(rs);
				closePrepareStatement(preStatement);
			} catch (SQLException exception) {
				throw new SQLException();
			}
		}
		return false;
	
	}

}
