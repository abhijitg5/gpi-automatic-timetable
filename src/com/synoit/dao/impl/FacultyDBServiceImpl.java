package com.synoit.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import com.synoit.dao.IFaculty;
import com.synoit.entity.Faculty;
import com.synoit.util.DBHandler;

public class FacultyDBServiceImpl extends DBHandler implements IFaculty {

	@Override
	public Integer insertFacultInfo(Faculty faculty) throws SQLException {
		PreparedStatement preStatement = null;
		ResultSet rs = null;
		Connection connection = null;
		try {
			String query = "";
			connection  = getConnection();
			query = QueryConstant.insertIntoFaculty();
			preStatement = connection.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
			preStatement.setString(1, faculty.getName());
			preStatement.setString(2, faculty.getDept());
			preStatement.setString(3, faculty.getSubject());
			preStatement.executeUpdate();
			rs = preStatement.getGeneratedKeys();
			if (rs.next()) {
				return rs.getInt(1);

			}

		} catch (Exception exception) {
			throw new SQLException();
		} finally {
			try {
				closeResultset(rs);
				closePrepareStatement(preStatement);
			} catch (SQLException exception) {
				throw new SQLException();
			}
		}
		return -1;
	}

	@Override
	public Integer updateFacultyInfo(Faculty faculty) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Integer deleteFacultyInfo(Faculty faculty) {
		// TODO Auto-generated method stub
		return null;
	}
	

}
