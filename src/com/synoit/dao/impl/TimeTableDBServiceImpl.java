package com.synoit.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import com.synoit.dao.ICreateTT;
import com.synoit.entity.TimeTable;
import com.synoit.util.DBHandler;

public class TimeTableDBServiceImpl extends DBHandler implements ICreateTT{

	@Override
	public ResultSet insertTimeTable(TimeTable timeTable) throws SQLException {

		PreparedStatement preStatement = null;
		ResultSet rs = null;
		Connection connection = null;
		try {
			String query = "";
			connection  = getConnection();
			
			if(timeTable.getDay().equalsIgnoreCase("MONDAY")){
				query = QueryConstant.updateTimeTableForMon();
			}else if(timeTable.getDay().equalsIgnoreCase("TUESDAY")){
				query = QueryConstant.updateTimeTableForTue();
			}else if(timeTable.getDay().equalsIgnoreCase("Wednesday")){
				query = QueryConstant.updateTimeTableForWed();
			}else if(timeTable.getDay().equalsIgnoreCase("Thursday")){
				query = QueryConstant.updateTimeTableForThu();
			}else if(timeTable.getDay().equalsIgnoreCase("Friday")){
				query = QueryConstant.updateTimeTableForFri();
			}else if(timeTable.getDay().equalsIgnoreCase("Saturday")){
				query = QueryConstant.updateTimeTableForSat();
			}
			
			preStatement = connection.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
			preStatement.setString(1,timeTable.getSubject().getSubCode());
			preStatement.setString(2,timeTable.getTimeSlot());
			preStatement.setString(3,timeTable.getDepartment().getName());
			preStatement.setString(4,timeTable.getSemister());
			preStatement.executeUpdate();
			rs = preStatement.getGeneratedKeys();

			if(rs !=null ){
				return rs;
			}
		} catch (Exception exception) {
			throw new SQLException();
		} finally {
			try {
				closeResultset(rs);
				closePrepareStatement(preStatement);
			} catch (SQLException exception) {
				throw new SQLException();
			}
		}
		return rs;
	
	}

	@Override
	public ResultSet getTableInfo(Connection connection, PreparedStatement preStatement, ResultSet rs,String dpt, String sem)
			throws SQLException {
		try{
			preStatement = connection.prepareStatement(QueryConstant.getTimeTableInfo());
			preStatement.setString(1,dpt);
			preStatement.setString(2, sem);
			rs = preStatement.executeQuery();
		} catch(Exception exception){
			throw new SQLException();
		} 
		return rs;
	}

}
