package com.synoit.dao;

import java.sql.SQLException;

import com.synoit.entity.Faculty;

public interface IFaculty{

	public Integer insertFacultInfo(Faculty faculty) throws SQLException;
	
	public Integer updateFacultyInfo(Faculty faculty) throws SQLException;
	
	public Integer deleteFacultyInfo(Faculty faculty) throws SQLException;
}
