package com.synoit.dao;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.synoit.entity.Subject;

public interface ISubject {

	public ResultSet insertSubjectInfo(Subject subject) throws SQLException;
	
	public Boolean updateSubjectInfo(Subject subject) throws SQLException;
	
	public Boolean deleteSubjectInfo(Subject subject) throws SQLException;
}
