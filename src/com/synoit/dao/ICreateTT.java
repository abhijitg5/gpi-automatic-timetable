package com.synoit.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.synoit.entity.TimeTable;

public interface ICreateTT {

	
	public ResultSet insertTimeTable(TimeTable timeTable) throws SQLException;
	
	public ResultSet getTableInfo(Connection connection, PreparedStatement preStatement, ResultSet rs, String dpt, String sem) throws SQLException;
}
