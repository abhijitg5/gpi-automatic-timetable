package com.synoit.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

public interface ISearchInfo {

	public List<String> getDepartment() throws SQLException;
	
	public List<String> getSemister() throws SQLException;
	
	public List<String> getSubject() throws SQLException;
	
	public List<String> getFaculty() throws SQLException;
	
	public List<String> getDay() throws SQLException;
	
	public List<String> getTimeSlot() throws SQLException;
	
	public ResultSet getSubjectInfo(Connection connection, PreparedStatement preStatement, ResultSet rs) throws SQLException;
	
	public ResultSet getFacultyInfo(Connection connection, PreparedStatement preStatement, ResultSet rs) throws SQLException;
}
