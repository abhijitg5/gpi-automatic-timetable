create database synoit;

---- t_department table -----------

create table t_department (id int(4) NOT NULL AUTO_INCREMENT ,
name varchar(30), PRIMARY KEY (id)
);

----- Facult table ----------

create table t_faculty (id int(4) NOT NULL AUTO_INCREMENT ,
name varchar(30),
department varchar(30),
subject varchar(30),
 PRIMARY KEY (id)
);

---------- semister ----------

create table t_semister (id int(4) NOT NULL AUTO_INCREMENT ,
semisterName varchar(30),
 PRIMARY KEY (id)
);

---------- Subject--------------------

create table t_subject (id int(4) NOT NULL AUTO_INCREMENT ,
subName varchar(30),
subCode varchar(30),
deptId int(5),
semId int(5),
 PRIMARY KEY (id),
  FOREIGN KEY (deptId)
        REFERENCES t_department(id),
  FOREIGN KEY (semId)
        REFERENCES t_semister(id)	
);

---------- deptSemSubMapping---------------not in use
create table t_deptSemSubMapping (id int(4) NOT NULL AUTO_INCREMENT ,
dptDId int(5),
semId int(5),
subjectId int(5),
 PRIMARY KEY (id),
  FOREIGN KEY (semId)
        REFERENCES t_semister(id),
    FOREIGN KEY (dptDId)
        REFERENCES t_department(id),
    FOREIGN KEY (subjectId)
        REFERENCES t_subject(id)        
        
);

---------- TimeTable---------------- not in use

create table t_timeTable (id int(4) NOT NULL AUTO_INCREMENT ,
facultyId int(5),
dssId int(5),
timeSlot varchar(30),
 PRIMARY KEY (id),
  FOREIGN KEY (facultyId)
        REFERENCES t_faculty(id),
  FOREIGN KEY (dssId)
        REFERENCES t_deptSemSubMapping(id)
);


create table t_timeTable_aa (id int(4) NOT NULL AUTO_INCREMENT ,
Time varchar(15),
Mon varchar(30),
Mon_SF varchar(30),
Tue varchar(30),
Tue_SF varchar(30),
Wed varchar(30),
Wed_SF varchar(30),
Thu varchar(30),
Thu_SF varchar(30),
Fri varchar(30),
Fri_SF varchar(30),
Sat varchar(30),
Sat_SF varchar(30),
Sun varchar(30),
Department varchar(30),
Semister varchar(30),
 PRIMARY KEY (id)
);

----------- Master Data--------
insert into t_department (name) value('Computer');
insert into t_department (name) value('Civil');
insert into t_department (name) value('Mechanical');
insert into t_department (name) value('Automobile');
insert into t_department (name) value('Electrical');
insert into t_department (name) value('Applied Science');

insert into t_semister (semisterName) value('Sem-I');
insert into t_semister (semisterName) value('Sem-II');
insert into t_semister (semisterName) value('Sem-III');
insert into t_semister (semisterName) value('Sem-IV');
insert into t_semister (semisterName) value('Sem-V');
insert into t_semister (semisterName) value('Sem-VI');

insert into t_timeTable_aa (Time, Department, Semister) values ('10:00 to 11:00',  'Computer', 'Sem-I' );
insert into t_timeTable_aa (Time, Department, Semister) values ('11:00 to 12:00' , 'Computer', 'Sem-I' );
insert into t_timeTable_aa (Time, Department, Semister) values ('12:45 to 01:45' , 'Computer', 'Sem-I' );
insert into t_timeTable_aa (Time, Department, Semister) values ('01:45 to 02:45' , 'Computer', 'Sem-I' );
insert into t_timeTable_aa (Time, Department, Semister) values ('03:00 to 04:00' , 'Computer', 'Sem-I' );
insert into t_timeTable_aa (Time, Department, Semister) values ('04:00 to 05:00' , 'Computer', 'Sem-I' );

insert into t_timeTable_aa (Time, Department, Semister) values ('10:00 to 11:00',  'Computer', 'Sem-II' );
insert into t_timeTable_aa (Time, Department, Semister) values ('11:00 to 12:00' , 'Computer', 'Sem-II' );
insert into t_timeTable_aa (Time, Department, Semister) values ('12:45 to 01:45' , 'Computer', 'Sem-II' );
insert into t_timeTable_aa (Time, Department, Semister) values ('01:45 to 02:45' , 'Computer', 'Sem-II' );
insert into t_timeTable_aa (Time, Department, Semister) values ('03:00 to 04:00' , 'Computer', 'Sem-II' );
insert into t_timeTable_aa (Time, Department, Semister) values ('04:00 to 05:00' , 'Computer', 'Sem-II' );

insert into t_timeTable_aa (Time, Department, Semister) values ('10:00 to 11:00',  'Computer', 'Sem-III' );
insert into t_timeTable_aa (Time, Department, Semister) values ('11:00 to 12:00' , 'Computer', 'Sem-III' );
insert into t_timeTable_aa (Time, Department, Semister) values ('12:45 to 01:45' , 'Computer', 'Sem-III' );
insert into t_timeTable_aa (Time, Department, Semister) values ('01:45 to 02:45' , 'Computer', 'Sem-III' );
insert into t_timeTable_aa (Time, Department, Semister) values ('03:00 to 04:00' , 'Computer', 'Sem-III' );
insert into t_timeTable_aa (Time, Department, Semister) values ('04:00 to 05:00' , 'Computer', 'Sem-III' );

insert into t_timeTable_aa (Time, Department, Semister) values ('10:00 to 11:00',  'Computer', 'Sem-IV' );
insert into t_timeTable_aa (Time, Department, Semister) values ('11:00 to 12:00' , 'Computer', 'Sem-IV' );
insert into t_timeTable_aa (Time, Department, Semister) values ('12:45 to 01:45' , 'Computer', 'Sem-IV' );
insert into t_timeTable_aa (Time, Department, Semister) values ('01:45 to 02:45' , 'Computer', 'Sem-IV' );
insert into t_timeTable_aa (Time, Department, Semister) values ('03:00 to 04:00' , 'Computer', 'Sem-IV' );
insert into t_timeTable_aa (Time, Department, Semister) values ('04:00 to 05:00' , 'Computer', 'Sem-IV' );

insert into t_timeTable_aa (Time, Department, Semister) values ('10:00 to 11:00',  'Computer', 'Sem-V' );
insert into t_timeTable_aa (Time, Department, Semister) values ('11:00 to 12:00' , 'Computer', 'Sem-V' );
insert into t_timeTable_aa (Time, Department, Semister) values ('12:45 to 01:45' , 'Computer', 'Sem-V' );
insert into t_timeTable_aa (Time, Department, Semister) values ('01:45 to 02:45' , 'Computer', 'Sem-V' );
insert into t_timeTable_aa (Time, Department, Semister) values ('03:00 to 04:00' , 'Computer', 'Sem-V' );
insert into t_timeTable_aa (Time, Department, Semister) values ('04:00 to 05:00' , 'Computer', 'Sem-V' );

insert into t_timeTable_aa (Time, Department, Semister) values ('10:00 to 11:00',  'Computer', 'Sem-VI' );
insert into t_timeTable_aa (Time, Department, Semister) values ('11:00 to 12:00' , 'Computer', 'Sem-VI' );
insert into t_timeTable_aa (Time, Department, Semister) values ('12:45 to 01:45' , 'Computer', 'Sem-VI' );
insert into t_timeTable_aa (Time, Department, Semister) values ('01:45 to 02:45' , 'Computer', 'Sem-VI' );
insert into t_timeTable_aa (Time, Department, Semister) values ('03:00 to 04:00' , 'Computer', 'Sem-VI' );
insert into t_timeTable_aa (Time, Department, Semister) values ('04:00 to 05:00' , 'Computer', 'Sem-VI' );
 

insert into t_timeTable_aa (Time, Department, Semister) values ('10:00 to 11:00',  'Civil', 'Sem-I' );
insert into t_timeTable_aa (Time, Department, Semister) values ('11:00 to 12:00' , 'Civil', 'Sem-I' );
insert into t_timeTable_aa (Time, Department, Semister) values ('12:45 to 01:45' , 'Civil', 'Sem-I' );
insert into t_timeTable_aa (Time, Department, Semister) values ('01:45 to 02:45' , 'Civil', 'Sem-I' );
insert into t_timeTable_aa (Time, Department, Semister) values ('03:00 to 04:00' , 'Civil', 'Sem-I' );
insert into t_timeTable_aa (Time, Department, Semister) values ('04:00 to 05:00' , 'Civil', 'Sem-I' );

insert into t_timeTable_aa (Time, Department, Semister) values ('10:00 to 11:00',  'Civil', 'Sem-II' );
insert into t_timeTable_aa (Time, Department, Semister) values ('11:00 to 12:00' , 'Civil', 'Sem-II' );
insert into t_timeTable_aa (Time, Department, Semister) values ('12:45 to 01:45' , 'Civil', 'Sem-II' );
insert into t_timeTable_aa (Time, Department, Semister) values ('01:45 to 02:45' , 'Civil', 'Sem-II' );
insert into t_timeTable_aa (Time, Department, Semister) values ('03:00 to 04:00' , 'Civil', 'Sem-II' );
insert into t_timeTable_aa (Time, Department, Semister) values ('04:00 to 05:00' , 'Civil', 'Sem-II' );

insert into t_timeTable_aa (Time, Department, Semister) values ('10:00 to 11:00',  'Civil', 'Sem-III' );
insert into t_timeTable_aa (Time, Department, Semister) values ('11:00 to 12:00' , 'Civil', 'Sem-III' );
insert into t_timeTable_aa (Time, Department, Semister) values ('12:45 to 01:45' , 'Civil', 'Sem-III' );
insert into t_timeTable_aa (Time, Department, Semister) values ('01:45 to 02:45' , 'Civil', 'Sem-III' );
insert into t_timeTable_aa (Time, Department, Semister) values ('03:00 to 04:00' , 'Civil', 'Sem-III' );
insert into t_timeTable_aa (Time, Department, Semister) values ('04:00 to 05:00' , 'Civil', 'Sem-III' );

insert into t_timeTable_aa (Time, Department, Semister) values ('10:00 to 11:00',  'Civil', 'Sem-IV' );
insert into t_timeTable_aa (Time, Department, Semister) values ('11:00 to 12:00' , 'Civil', 'Sem-IV' );
insert into t_timeTable_aa (Time, Department, Semister) values ('12:45 to 01:45' , 'Civil', 'Sem-IV' );
insert into t_timeTable_aa (Time, Department, Semister) values ('01:45 to 02:45' , 'Civil', 'Sem-IV' );
insert into t_timeTable_aa (Time, Department, Semister) values ('03:00 to 04:00' , 'Civil', 'Sem-IV' );
insert into t_timeTable_aa (Time, Department, Semister) values ('04:00 to 05:00' , 'Civil', 'Sem-IV' );

insert into t_timeTable_aa (Time, Department, Semister) values ('10:00 to 11:00',  'Civil', 'Sem-V' );
insert into t_timeTable_aa (Time, Department, Semister) values ('11:00 to 12:00' , 'Civil', 'Sem-V' );
insert into t_timeTable_aa (Time, Department, Semister) values ('12:45 to 01:45' , 'Civil', 'Sem-V' );
insert into t_timeTable_aa (Time, Department, Semister) values ('01:45 to 02:45' , 'Civil', 'Sem-V' );
insert into t_timeTable_aa (Time, Department, Semister) values ('03:00 to 04:00' , 'Civil', 'Sem-V' );
insert into t_timeTable_aa (Time, Department, Semister) values ('04:00 to 05:00' , 'Civil', 'Sem-V' );

insert into t_timeTable_aa (Time, Department, Semister) values ('10:00 to 11:00',  'Civil', 'Sem-VI' );
insert into t_timeTable_aa (Time, Department, Semister) values ('11:00 to 12:00' , 'Civil', 'Sem-VI' );
insert into t_timeTable_aa (Time, Department, Semister) values ('12:45 to 01:45' , 'Civil', 'Sem-VI' );
insert into t_timeTable_aa (Time, Department, Semister) values ('01:45 to 02:45' , 'Civil', 'Sem-VI' );
insert into t_timeTable_aa (Time, Department, Semister) values ('03:00 to 04:00' , 'Civil', 'Sem-VI' );
insert into t_timeTable_aa (Time, Department, Semister) values ('04:00 to 05:00' , 'Civil', 'Sem-VI' );

insert into t_timeTable_aa (Time, Department, Semister) values ('10:00 to 11:00',  'Mechanical', 'Sem-I' );
insert into t_timeTable_aa (Time, Department, Semister) values ('11:00 to 12:00' , 'Mechanical', 'Sem-I' );
insert into t_timeTable_aa (Time, Department, Semister) values ('12:45 to 01:45' , 'Mechanical', 'Sem-I' );
insert into t_timeTable_aa (Time, Department, Semister) values ('01:45 to 02:45' , 'Mechanical', 'Sem-I' );
insert into t_timeTable_aa (Time, Department, Semister) values ('03:00 to 04:00' , 'Mechanical', 'Sem-I' );
insert into t_timeTable_aa (Time, Department, Semister) values ('04:00 to 05:00' , 'Mechanical', 'Sem-I' );

insert into t_timeTable_aa (Time, Department, Semister) values ('10:00 to 11:00',  'Mechanical', 'Sem-II' );
insert into t_timeTable_aa (Time, Department, Semister) values ('11:00 to 12:00' , 'Mechanical', 'Sem-II' );
insert into t_timeTable_aa (Time, Department, Semister) values ('12:45 to 01:45' , 'Mechanical', 'Sem-II' );
insert into t_timeTable_aa (Time, Department, Semister) values ('01:45 to 02:45' , 'Mechanical', 'Sem-II' );
insert into t_timeTable_aa (Time, Department, Semister) values ('03:00 to 04:00' , 'Mechanical', 'Sem-II' );
insert into t_timeTable_aa (Time, Department, Semister) values ('04:00 to 05:00' , 'Mechanical', 'Sem-II' );

insert into t_timeTable_aa (Time, Department, Semister) values ('10:00 to 11:00',  'Mechanical', 'Sem-III' );
insert into t_timeTable_aa (Time, Department, Semister) values ('11:00 to 12:00' , 'Mechanical', 'Sem-III' );
insert into t_timeTable_aa (Time, Department, Semister) values ('12:45 to 01:45' , 'Mechanical', 'Sem-III' );
insert into t_timeTable_aa (Time, Department, Semister) values ('01:45 to 02:45' , 'Mechanical', 'Sem-III' );
insert into t_timeTable_aa (Time, Department, Semister) values ('03:00 to 04:00' , 'Mechanical', 'Sem-III' );
insert into t_timeTable_aa (Time, Department, Semister) values ('04:00 to 05:00' , 'Mechanical', 'Sem-III' );

insert into t_timeTable_aa (Time, Department, Semister) values ('10:00 to 11:00',  'Mechanical', 'Sem-IV' );
insert into t_timeTable_aa (Time, Department, Semister) values ('11:00 to 12:00' , 'Mechanical', 'Sem-IV' );
insert into t_timeTable_aa (Time, Department, Semister) values ('12:45 to 01:45' , 'Mechanical', 'Sem-IV' );
insert into t_timeTable_aa (Time, Department, Semister) values ('01:45 to 02:45' , 'Mechanical', 'Sem-IV' );
insert into t_timeTable_aa (Time, Department, Semister) values ('03:00 to 04:00' , 'Mechanical', 'Sem-IV' );
insert into t_timeTable_aa (Time, Department, Semister) values ('04:00 to 05:00' , 'Mechanical', 'Sem-IV' );

insert into t_timeTable_aa (Time, Department, Semister) values ('10:00 to 11:00',  'Mechanical', 'Sem-V' );
insert into t_timeTable_aa (Time, Department, Semister) values ('11:00 to 12:00' , 'Mechanical', 'Sem-V' );
insert into t_timeTable_aa (Time, Department, Semister) values ('12:45 to 01:45' , 'Mechanical', 'Sem-V' );
insert into t_timeTable_aa (Time, Department, Semister) values ('01:45 to 02:45' , 'Mechanical', 'Sem-V' );
insert into t_timeTable_aa (Time, Department, Semister) values ('03:00 to 04:00' , 'Mechanical', 'Sem-V' );
insert into t_timeTable_aa (Time, Department, Semister) values ('04:00 to 05:00' , 'Mechanical', 'Sem-V' );

insert into t_timeTable_aa (Time, Department, Semister) values ('10:00 to 11:00',  'Mechanical', 'Sem-VI' );
insert into t_timeTable_aa (Time, Department, Semister) values ('11:00 to 12:00' , 'Mechanical', 'Sem-VI' );
insert into t_timeTable_aa (Time, Department, Semister) values ('12:45 to 01:45' , 'Mechanical', 'Sem-VI' );
insert into t_timeTable_aa (Time, Department, Semister) values ('01:45 to 02:45' , 'Mechanical', 'Sem-VI' );
insert into t_timeTable_aa (Time, Department, Semister) values ('03:00 to 04:00' , 'Mechanical', 'Sem-VI' );
insert into t_timeTable_aa (Time, Department, Semister) values ('04:00 to 05:00' , 'Mechanical', 'Sem-VI' );

insert into t_timeTable_aa (Time, Department, Semister) values ('10:00 to 11:00',  'Automobile', 'Sem-I' );
insert into t_timeTable_aa (Time, Department, Semister) values ('11:00 to 12:00' , 'Automobile', 'Sem-I' );
insert into t_timeTable_aa (Time, Department, Semister) values ('12:45 to 01:45' , 'Automobile', 'Sem-I' );
insert into t_timeTable_aa (Time, Department, Semister) values ('01:45 to 02:45' , 'Automobile', 'Sem-I' );
insert into t_timeTable_aa (Time, Department, Semister) values ('03:00 to 04:00' , 'Automobile', 'Sem-I' );
insert into t_timeTable_aa (Time, Department, Semister) values ('04:00 to 05:00' , 'Automobile', 'Sem-I' );

insert into t_timeTable_aa (Time, Department, Semister) values ('10:00 to 11:00',  'Automobile', 'Sem-II' );
insert into t_timeTable_aa (Time, Department, Semister) values ('11:00 to 12:00' , 'Automobile', 'Sem-II' );
insert into t_timeTable_aa (Time, Department, Semister) values ('12:45 to 01:45' , 'Automobile', 'Sem-II' );
insert into t_timeTable_aa (Time, Department, Semister) values ('01:45 to 02:45' , 'Automobile', 'Sem-II' );
insert into t_timeTable_aa (Time, Department, Semister) values ('03:00 to 04:00' , 'Automobile', 'Sem-II' );
insert into t_timeTable_aa (Time, Department, Semister) values ('04:00 to 05:00' , 'Automobile', 'Sem-II' );

insert into t_timeTable_aa (Time, Department, Semister) values ('10:00 to 11:00',  'Automobile', 'Sem-III' );
insert into t_timeTable_aa (Time, Department, Semister) values ('11:00 to 12:00' , 'Automobile', 'Sem-III' );
insert into t_timeTable_aa (Time, Department, Semister) values ('12:45 to 01:45' , 'Automobile', 'Sem-III' );
insert into t_timeTable_aa (Time, Department, Semister) values ('01:45 to 02:45' , 'Automobile', 'Sem-III' );
insert into t_timeTable_aa (Time, Department, Semister) values ('03:00 to 04:00' , 'Automobile', 'Sem-III' );
insert into t_timeTable_aa (Time, Department, Semister) values ('04:00 to 05:00' , 'Automobile', 'Sem-III' );

insert into t_timeTable_aa (Time, Department, Semister) values ('10:00 to 11:00',  'Automobile', 'Sem-IV' );
insert into t_timeTable_aa (Time, Department, Semister) values ('11:00 to 12:00' , 'Automobile', 'Sem-IV' );
insert into t_timeTable_aa (Time, Department, Semister) values ('12:45 to 01:45' , 'Automobile', 'Sem-IV' );
insert into t_timeTable_aa (Time, Department, Semister) values ('01:45 to 02:45' , 'Automobile', 'Sem-IV' );
insert into t_timeTable_aa (Time, Department, Semister) values ('03:00 to 04:00' , 'Automobile', 'Sem-IV' );
insert into t_timeTable_aa (Time, Department, Semister) values ('04:00 to 05:00' , 'Automobile', 'Sem-IV' );

insert into t_timeTable_aa (Time, Department, Semister) values ('10:00 to 11:00',  'Automobile', 'Sem-V' );
insert into t_timeTable_aa (Time, Department, Semister) values ('11:00 to 12:00' , 'Automobile', 'Sem-V' );
insert into t_timeTable_aa (Time, Department, Semister) values ('12:45 to 01:45' , 'Automobile', 'Sem-V' );
insert into t_timeTable_aa (Time, Department, Semister) values ('01:45 to 02:45' , 'Automobile', 'Sem-V' );
insert into t_timeTable_aa (Time, Department, Semister) values ('03:00 to 04:00' , 'Automobile', 'Sem-V' );
insert into t_timeTable_aa (Time, Department, Semister) values ('04:00 to 05:00' , 'Automobile', 'Sem-V' );

insert into t_timeTable_aa (Time, Department, Semister) values ('10:00 to 11:00',  'Automobile', 'Sem-VI' );
insert into t_timeTable_aa (Time, Department, Semister) values ('11:00 to 12:00' , 'Automobile', 'Sem-VI' );
insert into t_timeTable_aa (Time, Department, Semister) values ('12:45 to 01:45' , 'Automobile', 'Sem-VI' );
insert into t_timeTable_aa (Time, Department, Semister) values ('01:45 to 02:45' , 'Automobile', 'Sem-VI' );
insert into t_timeTable_aa (Time, Department, Semister) values ('03:00 to 04:00' , 'Automobile', 'Sem-VI' );
insert into t_timeTable_aa (Time, Department, Semister) values ('04:00 to 05:00' , 'Automobile', 'Sem-VI' );
 
insert into t_timeTable_aa (Time, Department, Semister) values ('10:00 to 11:00',  'Electrical', 'Sem-I' );
insert into t_timeTable_aa (Time, Department, Semister) values ('11:00 to 12:00' , 'Electrical', 'Sem-I' );
insert into t_timeTable_aa (Time, Department, Semister) values ('12:45 to 01:45' , 'Electrical', 'Sem-I' );
insert into t_timeTable_aa (Time, Department, Semister) values ('01:45 to 02:45' , 'Electrical', 'Sem-I' );
insert into t_timeTable_aa (Time, Department, Semister) values ('03:00 to 04:00' , 'Electrical', 'Sem-I' );
insert into t_timeTable_aa (Time, Department, Semister) values ('04:00 to 05:00' , 'Electrical', 'Sem-I' );

insert into t_timeTable_aa (Time, Department, Semister) values ('10:00 to 11:00',  'Electrical', 'Sem-II' );
insert into t_timeTable_aa (Time, Department, Semister) values ('11:00 to 12:00' , 'Electrical', 'Sem-II' );
insert into t_timeTable_aa (Time, Department, Semister) values ('12:45 to 01:45' , 'Electrical', 'Sem-II' );
insert into t_timeTable_aa (Time, Department, Semister) values ('01:45 to 02:45' , 'Electrical', 'Sem-II' );
insert into t_timeTable_aa (Time, Department, Semister) values ('03:00 to 04:00' , 'Electrical', 'Sem-II' );
insert into t_timeTable_aa (Time, Department, Semister) values ('04:00 to 05:00' , 'Electrical', 'Sem-II' );

insert into t_timeTable_aa (Time, Department, Semister) values ('10:00 to 11:00',  'Electrical', 'Sem-III' );
insert into t_timeTable_aa (Time, Department, Semister) values ('11:00 to 12:00' , 'Electrical', 'Sem-III' );
insert into t_timeTable_aa (Time, Department, Semister) values ('12:45 to 01:45' , 'Electrical', 'Sem-III' );
insert into t_timeTable_aa (Time, Department, Semister) values ('01:45 to 02:45' , 'Electrical', 'Sem-III' );
insert into t_timeTable_aa (Time, Department, Semister) values ('03:00 to 04:00' , 'Electrical', 'Sem-III' );
insert into t_timeTable_aa (Time, Department, Semister) values ('04:00 to 05:00' , 'Electrical', 'Sem-III' );

insert into t_timeTable_aa (Time, Department, Semister) values ('10:00 to 11:00',  'Electrical', 'Sem-IV' );
insert into t_timeTable_aa (Time, Department, Semister) values ('11:00 to 12:00' , 'Electrical', 'Sem-IV' );
insert into t_timeTable_aa (Time, Department, Semister) values ('12:45 to 01:45' , 'Electrical', 'Sem-IV' );
insert into t_timeTable_aa (Time, Department, Semister) values ('01:45 to 02:45' , 'Electrical', 'Sem-IV' );
insert into t_timeTable_aa (Time, Department, Semister) values ('03:00 to 04:00' , 'Electrical', 'Sem-IV' );
insert into t_timeTable_aa (Time, Department, Semister) values ('04:00 to 05:00' , 'Electrical', 'Sem-IV' );

insert into t_timeTable_aa (Time, Department, Semister) values ('10:00 to 11:00',  'Electrical', 'Sem-V' );
insert into t_timeTable_aa (Time, Department, Semister) values ('11:00 to 12:00' , 'Electrical', 'Sem-V' );
insert into t_timeTable_aa (Time, Department, Semister) values ('12:45 to 01:45' , 'Electrical', 'Sem-V' );
insert into t_timeTable_aa (Time, Department, Semister) values ('01:45 to 02:45' , 'Electrical', 'Sem-V' );
insert into t_timeTable_aa (Time, Department, Semister) values ('03:00 to 04:00' , 'Electrical', 'Sem-V' );
insert into t_timeTable_aa (Time, Department, Semister) values ('04:00 to 05:00' , 'Electrical', 'Sem-V' );

insert into t_timeTable_aa (Time, Department, Semister) values ('10:00 to 11:00',  'Electrical', 'Sem-VI' );
insert into t_timeTable_aa (Time, Department, Semister) values ('11:00 to 12:00' , 'Electrical', 'Sem-VI' );
insert into t_timeTable_aa (Time, Department, Semister) values ('12:45 to 01:45' , 'Electrical', 'Sem-VI' );
insert into t_timeTable_aa (Time, Department, Semister) values ('01:45 to 02:45' , 'Electrical', 'Sem-VI' );
insert into t_timeTable_aa (Time, Department, Semister) values ('03:00 to 04:00' , 'Electrical', 'Sem-VI' );
insert into t_timeTable_aa (Time, Department, Semister) values ('04:00 to 05:00' , 'Electrical', 'Sem-VI' );

insert into t_timeTable_aa (Time, Department, Semister) values ('10:00 to 11:00',  'Applied Science', 'Sem-I' );
insert into t_timeTable_aa (Time, Department, Semister) values ('11:00 to 12:00' , 'Applied Science', 'Sem-I' );
insert into t_timeTable_aa (Time, Department, Semister) values ('12:45 to 01:45' , 'Applied Science', 'Sem-I' );
insert into t_timeTable_aa (Time, Department, Semister) values ('01:45 to 02:45' , 'Applied Science', 'Sem-I' );
insert into t_timeTable_aa (Time, Department, Semister) values ('03:00 to 04:00' , 'Applied Science', 'Sem-I' );
insert into t_timeTable_aa (Time, Department, Semister) values ('04:00 to 05:00' , 'Applied Science', 'Sem-I' );

insert into t_timeTable_aa (Time, Department, Semister) values ('10:00 to 11:00',  'Applied Science', 'Sem-II' );
insert into t_timeTable_aa (Time, Department, Semister) values ('11:00 to 12:00' , 'Applied Science', 'Sem-II' );
insert into t_timeTable_aa (Time, Department, Semister) values ('12:45 to 01:45' , 'Applied Science', 'Sem-II' );
insert into t_timeTable_aa (Time, Department, Semister) values ('01:45 to 02:45' , 'Applied Science', 'Sem-II' );
insert into t_timeTable_aa (Time, Department, Semister) values ('03:00 to 04:00' , 'Applied Science', 'Sem-II' );
insert into t_timeTable_aa (Time, Department, Semister) values ('04:00 to 05:00' , 'Applied Science', 'Sem-II' );

insert into t_timeTable_aa (Time, Department, Semister) values ('10:00 to 11:00',  'Applied Science', 'Sem-III' );
insert into t_timeTable_aa (Time, Department, Semister) values ('11:00 to 12:00' , 'Applied Science', 'Sem-III' );
insert into t_timeTable_aa (Time, Department, Semister) values ('12:45 to 01:45' , 'Applied Science', 'Sem-III' );
insert into t_timeTable_aa (Time, Department, Semister) values ('01:45 to 02:45' , 'Applied Science', 'Sem-III' );
insert into t_timeTable_aa (Time, Department, Semister) values ('03:00 to 04:00' , 'Applied Science', 'Sem-III' );
insert into t_timeTable_aa (Time, Department, Semister) values ('04:00 to 05:00' , 'Applied Science', 'Sem-III' );

insert into t_timeTable_aa (Time, Department, Semister) values ('10:00 to 11:00',  'Applied Science', 'Sem-IV' );
insert into t_timeTable_aa (Time, Department, Semister) values ('11:00 to 12:00' , 'Applied Science', 'Sem-IV' );
insert into t_timeTable_aa (Time, Department, Semister) values ('12:45 to 01:45' , 'Applied Science', 'Sem-IV' );
insert into t_timeTable_aa (Time, Department, Semister) values ('01:45 to 02:45' , 'Applied Science', 'Sem-IV' );
insert into t_timeTable_aa (Time, Department, Semister) values ('03:00 to 04:00' , 'Applied Science', 'Sem-IV' );
insert into t_timeTable_aa (Time, Department, Semister) values ('04:00 to 05:00' , 'Applied Science', 'Sem-IV' );

insert into t_timeTable_aa (Time, Department, Semister) values ('10:00 to 11:00',  'Applied Science', 'Sem-V' );
insert into t_timeTable_aa (Time, Department, Semister) values ('11:00 to 12:00' , 'Applied Science', 'Sem-V' );
insert into t_timeTable_aa (Time, Department, Semister) values ('12:45 to 01:45' , 'Applied Science', 'Sem-V' );
insert into t_timeTable_aa (Time, Department, Semister) values ('01:45 to 02:45' , 'Applied Science', 'Sem-V' );
insert into t_timeTable_aa (Time, Department, Semister) values ('03:00 to 04:00' , 'Applied Science', 'Sem-V' );
insert into t_timeTable_aa (Time, Department, Semister) values ('04:00 to 05:00' , 'Applied Science', 'Sem-V' );

insert into t_timeTable_aa (Time, Department, Semister) values ('10:00 to 11:00',  'Applied Science', 'Sem-VI' );
insert into t_timeTable_aa (Time, Department, Semister) values ('11:00 to 12:00' , 'Applied Science', 'Sem-VI' );
insert into t_timeTable_aa (Time, Department, Semister) values ('12:45 to 01:45' , 'Applied Science', 'Sem-VI' );
insert into t_timeTable_aa (Time, Department, Semister) values ('01:45 to 02:45' , 'Applied Science', 'Sem-VI' );
insert into t_timeTable_aa (Time, Department, Semister) values ('03:00 to 04:00' , 'Applied Science', 'Sem-VI' );
insert into t_timeTable_aa (Time, Department, Semister) values ('04:00 to 05:00' , 'Applied Science', 'Sem-VI' );


